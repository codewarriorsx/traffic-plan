package legs;

public class Coordinate {
	private int x, y;
	private double weight;
	private Coordinate previous;
	public Coordinate(int x, int y){
		this.x= x;
		this.y= y;
		weight= Double.MAX_VALUE;
	}
	public void serPreviousCoordnate(Coordinate prev){
		previous= prev;
	}
	public Coordinate getPreviousNode(){
		return previous;
	}
	public double getWeight(){
		return weight;
	}
	public void setWeight(double num){
		weight= num;
	}
	public void setCoordinate(int x,int y){
		this.x= x;
		this.y= y;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	public double getDistanceTowards(Coordinate b){
		return Math.sqrt(Math.pow(this.getX()-b.getX(),2)+ Math.pow(this.getY()- b.getY(), 2));
	}
	public int getDirectionTowards(Coordinate target){
		double angle= Math.toDegrees(Math.atan2(target.getY()-this.getY(),target.getX()-this.getX()));
		angle+= 90;
		//System.out.println(angle);
		if (angle< 0){
			angle+= 360;
		}
		int dir= -1;
		if (angle>= 0 && angle< 45){
			dir= 0;
		}
		else if (angle>= 45 && angle <90){
			dir= 1;
		}
		else if (angle>= 90 && angle< 135){
			dir =2;
		}
		else if (angle>= 135 && angle < 180){
			dir= 3;
		}
		else if (angle >= 180 && angle< 225){
			dir= 4;
		}
		else if (angle>= 225 && angle < 270){
			dir= 5;
		}
		else if (angle>= 270 && angle< 315){
			dir= 6;
		}
		else if (angle>= 315 && angle< 360){
			dir= 7;
		}
		return dir;
	}
	public String toString(){
		return "["+ x+ ","+ y+ "]";
	}
}
