package legs;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImagePanel extends JPanel{ //Class Reference: http://stackoverflow.com/questions/11464791/how-to-add-image-in-jpanel
    private BufferedImage image;
    public ImagePanel(String imagePath) {
       try {                
          image = ImageIO.read(new File(imagePath));
       } catch (IOException ex) {
            // handle exception...
    	   System.out.println("Image not found" + imagePath);
       }
    }
    public ImagePanel(BufferedImage importImage){
    	image= importImage;
    }
    public BufferedImage getBufferedImage(){
    	return image;
    }
    public void paintComponent(Graphics g) {//Refernce http://stackoverflow.com/questions/4533526/how-to-center-align-background-image-in-jpanel
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		int x = (this.getWidth() - image.getWidth(null)) / 2;
		int y = (this.getHeight() - image.getHeight(null)) / 2;
		g2d.drawImage(image, x, y, null);
    }
}