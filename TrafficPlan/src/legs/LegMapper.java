package legs;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Random;

//AUTHOR: Alan Wang

public class LegMapper extends JFrame{
	ImagePanel mapImage;
	BufferedImage buffMapImage;
	int directions[]= {0,1,2,3,4,5,6,7}; //n= 0, ne= 1, e= 2, se= 3, s= 4, sw= 5, w= 6, nw= 7
	Random randall;
	ArrayList<Coordinate> path= new ArrayList<Coordinate>();
	ArrayList<Coordinate> queue;
	ArrayList<Coordinate> bigpath= new ArrayList<Coordinate>();
	
	public LegMapper(){
		randall= new Random();
		this.setSize(700,700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel mapFrame= new JPanel();
		mapFrame.setLayout(new BorderLayout());
		mapImage= new ImagePanel("/Users/wangalan/Desktop/map4.jpg");
		buffMapImage= mapImage.getBufferedImage();
		mapFrame.add(mapImage, BorderLayout.CENTER);
		//thresholdtest();
		getShortestPath(new Coordinate(86,74), new Coordinate(619, 633), 30); //660,90: 80,780
		//ArrayList<Coordinate> apath= getShortestPath(new Coordinate(660, 90), new Coordinate(80, 780), 30);
		
		this.add(mapFrame);
		this.setVisible(true);
	}
	public LegMapper(BufferedImage staticMapImage){
		randall= new Random();
		this.setSize(700,700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel mapFrame= new JPanel();
		mapFrame.setLayout(new BorderLayout());
		mapImage= new ImagePanel(staticMapImage);
		buffMapImage= mapImage.getBufferedImage();
		mapFrame.add(mapImage, BorderLayout.CENTER);
		//thresholdtest();
		this.add(mapFrame);
		this.setVisible(true);
		
	}
	
	public boolean canMoveInDir(Coordinate a, int dir){
		Coordinate b= moveInDir(a, dir);
		if (b.getX()>= 0 && b.getX()< mapImage.getBufferedImage().getWidth() && b.getY()>= 0 && b.getY()< mapImage.getBufferedImage().getHeight()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public Coordinate moveInDir(Coordinate dim, int dir){ //Might return an invalid location! Careful
		int x= dim.getX();
		int y= dim.getY();
		if (dir== 0){
			y--;
		}
		else if (dir== 1){
			y--;
			x++;
		}
		else if (dir== 2){
			x++;
		}
		else if (dir== 3){
			x++;
			y++;
		}
		else if (dir== 4){
			y++;
		}
		else if (dir== 5){
			y++;
			x--;
		}
		else if (dir== 6){
			x--;
		}
		else if (dir== 7){
			x--;
			y--;
		}
		return new Coordinate(x, y);
	}
	public void findShortestPath(){
		PriorityQueue<Coordinate> shortestPath= new PriorityQueue<Coordinate>();
		
	}
	
	public class CoordinateComparator implements Comparator<Coordinate>{

		@Override
		public int compare(Coordinate o1, Coordinate o2) {
			// TODO Auto-generated method stub
			if (o1.getWeight()> o2.getWeight()){
				return 1;
			}
			else{
				return -1;
			}
		}
		
	}
	
	public ArrayList<Coordinate> getShortestPath(Coordinate start, Coordinate end, int numPointsWanted){
		Color chosen= new Color(buffMapImage.getRGB(start.getX(), start.getY()));
		int threshold= 30;
		
		double[][] mapMarked= new double[buffMapImage.getWidth()][buffMapImage.getHeight()];
		for (int x= 0; x< buffMapImage.getWidth(); x++){
			for (int y= 0; y< buffMapImage.getHeight(); y++){
				mapMarked[x][y]= -1;
			}
		}
		
		ArrayList<Coordinate> shortestPath= new ArrayList<Coordinate>();
		PriorityQueue<Coordinate> pQueue= new PriorityQueue<Coordinate>(100, new CoordinateComparator());
		Coordinate s= new Coordinate(start.getX(), start.getY());
		//Find all surrounding coordinates, changing the weight on each potential path equal to the distance from the current node to the surrounding one, if that value is less than what is on the current node
		//When you process an item in the queue, mark it as set after running a for loop through all it's surrounding nodes, setting each of their distances
		
		s.setWeight(0);
		pQueue.add(s);
		boolean flag= false;
		while(pQueue.size()> 0 && flag== false){
			Coordinate current= pQueue.peek();
			//System.out.println(current);
			if (isEqual(current, end)){
				System.out.println("Found end!");
				flag= true;
				while(current.getPreviousNode()!= null){
					buffMapImage.setRGB(current.getX(), current.getY(), new Color (0,255,0).getRGB());
					shortestPath.add(current);
					current= current.getPreviousNode();
				}
				break;
			}
			pQueue.remove();
			mapMarked[current.getX()][current.getY()]= 1;
			//buffMapImage.setRGB(current.getX(), current.getY(), new Color(0,0,255).getRGB());
			ArrayList<Coordinate> surrounding= getSurroundingPossibleCoordinates(current, mapMarked, chosen, threshold);
			for (int x= 0; x< surrounding.size(); x++){ //Setting the weight of the corresponding nodes
				if (surrounding.get(x).getWeight()> current.getDistanceTowards(surrounding.get(x))+ current.getWeight()){ //if weight on surrounding chosen node is larger
					surrounding.get(x).serPreviousCoordnate(current);
					surrounding.get(x).setWeight(current.getDistanceTowards(surrounding.get(x))+ current.getWeight());
					pQueue.add(surrounding.get(x));
					mapMarked[surrounding.get(x).getX()][surrounding.get(x).getY()]= 1;
					System.out.println(current.getDistanceTowards(surrounding.get(x))+ current.getWeight());
					if (current.getDistanceTowards(surrounding.get(x))+ current.getWeight()> 15){
						//flag= true;
					}
				}
			}
		}
		System.out.println(shortestPath);
		ArrayList<Coordinate> filteredPath= new ArrayList<Coordinate>();
		for (int x= 0; x< shortestPath.size(); x++){
			if (x%(shortestPath.size()/numPointsWanted)== 0){
				filteredPath.add(shortestPath.get(x));
				buffMapImage.setRGB(shortestPath.get(x).getX(), shortestPath.get(x).getY(), new Color(0,0,255).getRGB());
			}
		}
		
		return filteredPath;
	}
	
	
	public void markTentativeDistances(double[][] map, ArrayList<Coordinate> possiblePaths, Coordinate start){
		for (int x= 0; x< possiblePaths.size(); x++){
			//If the marked distance is -1 OR is larger than the current chosen path (which probably will never happen in this case
			if (map[possiblePaths.get(x).getX()][possiblePaths.get(x).getY()]== -1 ){
				map[possiblePaths.get(x).getX()][possiblePaths.get(x).getY()]= map[start.getX()][start.getY()]+ possiblePaths.get(x).getDistanceTowards(start);
				buffMapImage.setRGB(possiblePaths.get(x).getX(), possiblePaths.get(x).getY(), new Color(0,0,(int) (map[start.getX()][start.getY()]+ possiblePaths.get(x).getDistanceTowards(start)%255)).getRGB());
			}
		}
	}
	public ArrayList<Coordinate> sortArrayList(ArrayList<Coordinate> possiblePaths, Coordinate start){
		ArrayList<Coordinate> sorted= new ArrayList<Coordinate>();
		if (possiblePaths.size()> 0){
			for (int y= 0; y< possiblePaths.size(); y++){
				Coordinate chosen= possiblePaths.get(0);
				double shortestPath= possiblePaths.get(0).getDistanceTowards(start);
				for (int x= 1; x< possiblePaths.size(); x++){
					//Find smallest
					if (shortestPath> possiblePaths.get(x).getDistanceTowards(start)){
						shortestPath= possiblePaths.get(x).getDistanceTowards(start);
					}
				}
				possiblePaths.remove(chosen);
				System.out.print(" "+ chosen.getDistanceTowards(start));
				sorted.add(chosen);
			}
		}
		System.out.println();
		
		return sorted;
	}
	
	public ArrayList<Coordinate> getSurroundingPossibleCoordinates(Coordinate a, double marked[][], Color chosen, int threshold){
		ArrayList<Coordinate> possiblePaths= new ArrayList<Coordinate>();
		for (int x= 0; x< 8; x++){
			if (canMoveInDir(a, directions[x])){// if it is valid
				Coordinate poss= moveInDir(a, directions[x]);
				if (isThresholdColor(getColor(poss), chosen, threshold)){ //If it is of the right color
					if (marked[poss.getX()][poss.getY()]== -1){//If the coordinate is unmarked
						possiblePaths.add(poss);
						//System.out.println(poss.getX()+ ","+ poss.getY()+ ": "+ moveDistance);
						//drawCoordinate(poss, 0, (int) (marked[poss.getX()][poss.getY()]%255), 0); //Draw passed with green
					}
				}
			}
		}
		return possiblePaths;
	}
	
	public void getSurroundingInThresholdValidCoordinates(ArrayList<Coordinate> list, double marked[][], Coordinate current,  Color chosen, int threshold){
		double moveDistance= marked[current.getX()][current.getY()];
		for (int x= 0; x< 8; x++){
			if (canMoveInDir(current, directions[x])){// if it is valid
				Coordinate poss= moveInDir(current, directions[x]);
				if (isThresholdColor(getColor(poss), chosen, threshold)){ //If it is of the right color
					if (marked[poss.getX()][poss.getY()]== 0){//If the coordinate is unmarked
						list.add(poss);
						marked[poss.getX()][poss.getY()]= moveDistance+ poss.getDistanceTowards(current);
						//System.out.println(poss.getX()+ ","+ poss.getY()+ ": "+ moveDistance);
						drawCoordinate(poss, 0, (int) (marked[poss.getX()][poss.getY()]%255), 0); //Draw passed with green
						//drawCoordinate(poss, 0, 0, 0);
						bigpath.add(poss);
				
					}
				}
			}
		}
	}

	public boolean isEqual(Coordinate a, Coordinate b){
		if (a.getX() == b.getX() && a.getY()== b.getY()){
			return true;
		}
		else{
			return false;
		}
	}
	public Color getColor(Coordinate a){
		return new Color(buffMapImage.getRGB(a.getX(), a.getY()));
	}
	
	public void drawCoordinate(Coordinate a, int R, int G, int B){
		buffMapImage.setRGB(a.getX(), a.getY(), new Color(R,G,B).getRGB());
	}
	
	public int getClosestDir(int dir){
		int chose= randall.nextInt(2);
		if (chose== 0){
			dir--;
		}
		else{
			dir++;
		}
		
		if (dir< 0){
			dir= 7;
		}
		return dir;
	}
	
	
	public boolean isThresholdColor(Color inquestion, Color chosen, int threshold){
		int r1= inquestion.getRed();
		int g1= inquestion.getGreen();
		int b1= inquestion.getBlue();
		
		int r2= chosen.getRed();
		int g2= chosen.getGreen();
		int b2= chosen.getBlue();
		//System.out.println("R: "+ r1 + " G: "+ g1+ "B:"+ b1);
		//System.out.println("R: "+ r2 + " G: "+ g2+ "B:"+ b2);
		
		//System.out.print("Diff: "+Math.sqrt(Math.pow(r1-r2, 2) + Math.pow(g1-g2, 2)+ Math.pow(b1-b2, 2)));
		if (Math.sqrt(Math.pow(r1-r2, 2) + Math.pow(g1-g2, 2)+ Math.pow(b1-b2, 2))< threshold){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	
	public void thresholdtest(){
		for (int x= 0; x< mapImage.getBufferedImage().getWidth(); x++){
			for (int y= 0; y< mapImage.getBufferedImage().getHeight(); y++){
				Color c= new Color(mapImage.getBufferedImage().getRGB(x, y));
				//System.out.print("R: "+ c.getRed()+ "G: "+ c.getGreen() + "B: "+ c.getBlue());
				if (isThresholdColor(c, new Color(0,0,255), 10)){
					System.out.println("Blue" + "["+x + ","+ y+ "]");
				}
			}
			//System.out.println();
		}
	}
	
	public static void main(String[] args){
		new LegMapper();
	}
}
