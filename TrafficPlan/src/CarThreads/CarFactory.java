package CarThreads;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import Map.LatLongPoint;
import Map.Map;
import Map.GeoTools;

public class CarFactory {
	public ArrayList<CarThread> cars;
	public Map panel;
	ImageIcon image = new ImageIcon("frog.png");
	ImageIcon image2 = new ImageIcon("frog2.png");
	
	public CarFactory(Map panel){
		cars = new ArrayList<CarThread>();
		this.panel = panel;
	}
	
	public void addCar(CarThread car){
		cars.add(car);
		car.start();
	}
	
	public void removeCar(CarThread car){
		cars.remove(car);
		
	}
	
	
	public void carDraw(Graphics g){
        java.awt.Point xy = null, xy2= null;
        //System.out.println(cars.size());
		for(int i=0; i<cars.size(); i++){
			CarThread car = cars.get(i);
			g.setColor(car.getColor());
			boolean isDisplay = true;
//			if(cars.size() > 1){
//				for(int j=i+1; j<cars.size(); j++){
//					CarThread other = cars.get(j);
//					if(car.getCarPos() != null && other.getCarPos() != null){
//						xy = GeoTools.convertLatLongToLocalXY(car.getCarPos(),panel);
//						xy2 = GeoTools.convertLatLongToLocalXY(other.getCarPos(),panel);
//						double diffX = xy.getX()- xy2.getX();
//						double diffY = xy.getY()- xy2.getY();
//						double dist = Math.sqrt(diffX*diffX + diffY*diffY);
//						if(dist < 30.0){
//							isDisplay = false;
//						}
//					}
//				}	
//			}
			if(isDisplay){
				
				if(car.getCarPos() != null){
					xy = GeoTools.convertLatLongToLocalXY(car.getCarPos(),panel);
					Double d= new Double(xy.getX()- 8);
					int I= d.intValue();
					Double d2= new Double(xy.getY()- 8);
					int I2= d2.intValue();
					
					if(car.isSpeeding == true){
						g.drawImage(image.getImage(), I-8, I2-8, 16, 16, null);
					}
					else if(car.isSpeeding == false){
						g.drawImage(image2.getImage(), I-8, I2-8, 16, 16, null);
					}
					
					
					//g.fillRect(I-8, I2-8, 16, 16);
				}
				//g.fillOval((panel.getMapWidth()/2), (panel.getMapHeight()/2), 10, 10);
				
				
//				LatLongPoint testPoint = new LatLongPoint(34.030876, -118.433504);
//				Point test = GeoTools.convertLatLongToLocalXY(testPoint, panel);
//				
//				Double t= new Double(test.getX()-8);
//				int T1= t.intValue();
//				Double t2= new Double(test.getY()-8);
//				int T2=t2.intValue();
//				g.setColor(Color.PINK);
//				g.fillOval(T1, T2, 10, 10);
//
//
//
//				LatLongPoint testPoint2 = new LatLongPoint(34.055197, -118.214214);
//				Point test2 = GeoTools.convertLatLongToLocalXY(testPoint2, panel);
//				
//				Double s= new Double(test2.getX()-8);
//				int S1= s.intValue();
//				Double s2= new Double(test2.getY()-8);
//				int S2=s2.intValue();
//				g.setColor(Color.BLUE);
//				g.fillOval(S1, S2, 10, 10);

			}
		}
	}


	
}