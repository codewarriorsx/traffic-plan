package CarThreads;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Vector;

import universe.Brain;
import Algorithm.Ramp;
import Algorithm.RampParser;
import Map.LatLongPoint;

public class CarThread extends Thread{

		private long carID;
        private double speed;
        private int posX, posY;
        private Color col;
        private double avgSpeed;
        private boolean isDraw;
        private int direction = 1;
        private LatLongPoint currPos;
        private LatLongPoint nextPos;
        private LatLongPoint carPos;

        private Vector<Ramp> FreeWay;
        private String freeWay;
        
        public int FreeWayIndex = 0;
        public boolean isSpeeding;

        double diffLa;
        double diffLo;

        double stepLa;
        double stepLo;
        double distance;

        CarFactory carFactory;
        
        boolean bExit = false;
        
        public Vector<Ramp> I10;
    	public	Vector<Ramp> I101;
    	public	Vector<Ramp> I405;
    	public	Vector<Ramp> I105;
    	
    	RampParser rp;


        public CarThread(){

        }

        public CarThread(RampParser rp, String freeWay, String start, int direction, double speed, CarFactory carFactory, long carID){
        		carPos = new LatLongPoint(45,45);
        		this.rp=rp;// ramp parser
    			I10 = rp.getI10();// load all of the nodes into the ramp
    			I101 = rp.getI101();
    			I405 = rp.getI405();
    			I105 = rp.getI105();
    			this.freeWay = freeWay;
    			if(freeWay.equals("10")){
    				this.setFreeWay(I10);
    				
    			}
    			else if(freeWay.equals("101")){
    				this.setFreeWay(I101);
    			}
    			else if(freeWay.equals("405")){
    				this.setFreeWay(I405);
    			}
    			else if(freeWay.equals("105")){
    				this.setFreeWay(I105);
    			}
    			
                this.setSpeed(speed);
                this.setDirection(direction);
                this.setStartPoint(start);
                this.setAvgSpeed(speed);
                this.carFactory = carFactory;
                this.setCarID(carID);
                
                

    			
        }

        public void setCar(String freeWay, String start, int direction, double speed){
        	
        	   if(freeWay.equals("10")){
   				this.setFreeWay(I10);
   				}
   				else if(freeWay.equals("101")){
   					this.setFreeWay(I101);
   				}
	   			else if(freeWay.equals("405")){
	   				this.setFreeWay(I405);
	   			}
	   			else if(freeWay.equals("105")){
	   				this.setFreeWay(I105);
	   			}
                this.setSpeed(speed);
                this.setDirection(direction);
                this.setStartPoint(start);
                
             


        }
        
        public void exit(boolean bExit){
            this.bExit = bExit;
        }
        
        public void setCarID(long carID){
        	this.carID = carID;
        }
        

        public void setStartPoint(String start){

                for(int i=0; i<FreeWay.size(); i++){
                        Ramp r = FreeWay.get(i);
                        if(start.equals(r.getRampName())){
                                FreeWayIndex = i;
                                //System.out.println(r.getRampName());

                        }
                }
                
                if((FreeWayIndex+direction) < 0 || (FreeWayIndex+direction) >= FreeWay.size()){
                	//setPosition(-1000,-1000);

                     carPos.setLongitude(-115.175);
                    carPos.setLatitude(36.125);

                	setSpeed(0);
                	System.out.println("DEAD Thread: " + this.speed + " " + this.getPosX() + " " + this.getPosY());
                		
                        //carFactory.removeCar(this);
                        exit(true);
      
                        //Brain.carDic.remove(this.getId());
                }
               else{
                        currPos = new LatLongPoint(this.FreeWay.get(FreeWayIndex).getLatLongPoint().getLatitude(), this.FreeWay.get(FreeWayIndex).getLatLongPoint().getLongitude());
                        nextPos = new LatLongPoint(this.FreeWay.get(FreeWayIndex+direction).getLatLongPoint().getLatitude(), this.FreeWay.get(FreeWayIndex+direction).getLatLongPoint().getLongitude());
                        distance = Math.sqrt((nextPos.getLatitude()-currPos.getLatitude())*(nextPos.getLatitude()-currPos.getLatitude()))+((nextPos.getLongitude()-currPos.getLongitude())*(nextPos.getLongitude()-currPos.getLongitude()));

                        double x = (nextPos.getLongitude()-currPos.getLongitude())* Math.cos((nextPos.getLatitude()+currPos.getLatitude())/2);
                        double y = nextPos.getLatitude() - currPos.getLatitude();
                        double distance2 = Math.sqrt(x*x+y*y)*3959;

                        double steps = (distance2/57.3447)/speed;

                        diffLa = (nextPos.getLatitude() - currPos.getLatitude())/steps;
                        diffLo = (nextPos.getLongitude() - currPos.getLongitude())/steps;

                        stepLa = diffLa/(1.0);
                        stepLo = diffLo/(1.0);
                        //carPos = currPos;
                        //carPos = new LatLongPoint(currPos.getLatitude(), currPos.getLongitude());
                        carPos.setLongitude(currPos.getLongitude());
                        carPos.setLatitude(currPos.getLatitude());
                        //System.out.println(carPos.getLatitude()+", "+carPos.getLongitude());
                }
        }

        public void setFreeWay(Vector<Ramp> FreeWay){
                this.FreeWay = FreeWay;
        }

        public void setSpeed(double speed){

                this.speed = speed/3600/1000*50;

        }

        public void setDirection(int direction){
                this.direction = direction;
        }


        public void visible(boolean isDraw){
                this.isDraw = isDraw;
        }

        public boolean isDraw(){
                return isDraw;
        }

        public void setAvgSpeed(double avgSpeed){
                this.avgSpeed = avgSpeed;
                setColor();
        }


        public void setPosition(int posX, int posY){
                this.posX = posX;
                this.posY = posY;
        }

        public int getPosX(){
                return posX;
        }

        public int getPosY(){
                return posY;
        }

        public double getLatitude(){
                return carPos.getLatitude();
        }

        public double getLongitude(){
                return carPos.getLongitude();
        }

        private void setColor(){
                if(avgSpeed > 70){
                        col = Color.GREEN;
                        isSpeeding = true;
                }
                if(avgSpeed < 70){
                        col = Color.RED;
                        isSpeeding = false;
                }
        }

        public Color getColor(){
                return col;
        }


        public void run(){
                while(!bExit){
      
                                carPos.setLatitude(carPos.getLatitude()+stepLa);
                                carPos.setLongitude(carPos.getLongitude()+stepLo);

                                if(Math.abs(carPos.getLatitude()-nextPos.getLatitude()) < Math.abs(stepLa)*5 ||
                                		Math.abs(carPos.getLongitude()-nextPos.getLongitude()) < Math.abs(stepLo)*5){

                                        FreeWayIndex += direction;

                                        if(FreeWayIndex+direction < 0 || FreeWayIndex+direction >= FreeWay.size()){
                                                //carFactory.removeCar(this);
                                        		//setPosition(-1000,-1000);
                                            carPos.setLongitude(-115.175);
                                            carPos.setLatitude(36.125);
                                        		setSpeed(0);
                                                exit(true);
                                        }
                                        
                                    
                                        else{
                                        	 currPos = new LatLongPoint(this.FreeWay.get(FreeWayIndex).getLatLongPoint().getLatitude(), this.FreeWay.get(FreeWayIndex).getLatLongPoint().getLongitude());
                                             nextPos = new LatLongPoint(this.FreeWay.get(FreeWayIndex+direction).getLatLongPoint().getLatitude(), this.FreeWay.get(FreeWayIndex+direction).getLatLongPoint().getLongitude());
                                             
                                                carPos = new LatLongPoint(currPos.getLatitude(),currPos.getLongitude());

                                                distance = Math.sqrt((nextPos.getLatitude()-currPos.getLatitude())*(nextPos.getLatitude()-currPos.getLatitude()))+((nextPos.getLongitude()-currPos.getLongitude())*(nextPos.getLongitude()-currPos.getLongitude()));

                                                double x = (nextPos.getLongitude()-currPos.getLongitude())* Math.cos((nextPos.getLatitude()+currPos.getLatitude())/2);
                                                double y = nextPos.getLatitude() - currPos.getLatitude();
                                                double distance2 = Math.sqrt(x*x+y*y)*3959;

                                                double steps = (distance2/57.3447)/speed;

                                                diffLa = (nextPos.getLatitude() - currPos.getLatitude())/steps;
                                                diffLo = (nextPos.getLongitude() - currPos.getLongitude())/steps;

                                                stepLa = diffLa/(1.0);
                                                stepLo = diffLo/(1.0);


                                        }
                                }

                        try {
                                Thread.sleep(50);
                        } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                        } // sleep for between 0 and 10 seconds
                }
        }

    public LatLongPoint getCarPos() {
        return carPos;
    }
    
    public Vector<Ramp> getFreeway(){
    	
    	 if(freeWay.equals("10")){
			return I10;
		}
		else if(freeWay.equals("101")){
			return I101;
		}
	   	else if(freeWay.equals("405")){
	   		return I405;
	   	}
	   	else if(freeWay.equals("105")){
	   		return I105;
	   	}
	   	else{
	   		return null;
	   	}
    }
}