package gui;

import Algorithm.Dijkstra;
import Algorithm.Ramp;
import Algorithm.RampParser;
import Map.TwitterDispatcher;
import javax.swing.*;
import universe.Brain;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

//AUTHOR: Alan Wang

public class Gui extends JFrame{
	JPanel jpMainFrame;//This is the "frame" that contains all of the components
	JPanel sideFrame;//Inside the panel contains all the side bar panels (i.e., the twitter frame, and more to come)
	JPanel mapFrame, graphFrame; //The frame that contains the map api
	JPanel tableFrame;
	JPanel twitterFrame; //The frame that contains the twitter api
	JPanel additionSideFrame; //Frame contains everything else we want in the side bar (placeholder for now?)
	JMenuBar menuBar;//This is the menu bar lol
	JMenu jmAbout, jmOptions; //This is the menu in the menu bar
	JMenuItem jmAbout1, jmiOption1; //Items in about, and items in Option
	JTabbedPane jtpTabMapGraph;//Tabbed pane for map and one for graph
	JTabbedPane graphPane;
	Graphs lineChart;
	HistoricalTable historicalTable;
	
	static JTextArea jtaMessage, jtaTwitterMessage;
	static String[] namesOfRamps= new String[]{};
	WideComboBox jcStreetChose1, jcStreetChose2;
	int i = 0;
	
	
	public Gui(){
		
		super ("Transitional Taddle Tale Traffic Tracker");

		importNamesOfRampes();// Taken from Jason's file
		setSize(1000, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
		setVisible(true); //Remove when done testing
		appendToTwitterTextArea("Sample!");
		addGraphToGraphPanel();	
		addLineChart();
		addHistoricalTable();
	}
	
/*//WE NEED THIS
	public void draw(){
		repaint();
	}*/
	
	public static void appendToTwitterTextArea(String info){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		jtaTwitterMessage.append("["+ dateFormat.format(date)+"]: "+ info+ "\n");
		jtaTwitterMessage.setCaretPosition(jtaTwitterMessage.getDocument().getLength());
		jtaTwitterMessage.getRootPane().revalidate();
	}
	
	public static void appendToFastestPathTextArea(String info){
		//jtaMessage.append("Fastest path calculation: "+ "\n");
		jtaMessage.append(info + "\n");
		jtaMessage.setCaretPosition(jtaMessage.getDocument().getLength());
		jtaMessage.getRootPane().revalidate();
	}
	
	public JPanel createTwitterPanel(){
		JPanel twitterPanel= new JPanel();
		twitterPanel.setLayout(new BorderLayout());
		
		JLabel jlTwitterLabel= new JLabel("TattleTweet@TattleTwitTweet:");
		jlTwitterLabel.setBorder(BorderFactory.createEtchedBorder());
		twitterPanel.add(jlTwitterLabel, BorderLayout.NORTH);
		jtaTwitterMessage= new JTextArea();
		jtaTwitterMessage.setEditable(false);
		jtaTwitterMessage.setBackground(jpMainFrame.getBackground());
		jtaTwitterMessage.setFont(new Font("Arial", Font.PLAIN, 10));
		twitterPanel.add(new JScrollPane(jtaTwitterMessage));
		
		return twitterPanel;
	}
	
	public JPanel createUserInputPanel(String[] listOfNodeNames){
		JPanel jpUserPanel= new JPanel();
		jpUserPanel.setLayout(new GridBagLayout());
		
		JLabel jlFrom= new JLabel("From: ", SwingConstants.LEFT);
		JLabel jlTo= new JLabel("To: ", SwingConstants.LEFT);
		jcStreetChose1= new WideComboBox(namesOfRamps);
		jcStreetChose2= new WideComboBox(namesOfRamps);
		
		JButton jbFindFastestPath= new JButton("Find fastest path");
		jbFindFastestPath.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                Dijkstra jason = new Dijkstra();
                Vector<Ramp> bestPath = new Vector<Ramp>();
                double time = jason.calcBestPath(RampParser.Ramps, RampParser.rampDictionary.get(jcStreetChose1.getSelectedItem()), RampParser.rampDictionary.get(jcStreetChose2.getSelectedItem()), bestPath);
                //String[] path= (String[]) bestPath.toArray();
                appendToFastestPathTextArea("Fastest path from " + jcStreetChose1.getSelectedItem() + " to " + jcStreetChose2.getSelectedItem() + ": ");
                for (int x = 0; x < bestPath.size(); x++) {
                    appendToFastestPathTextArea("    " + x + ") I" +bestPath.get(x).getFreewayNumber()+ " " + bestPath.get(x).getRampName());
                }
                appendToFastestPathTextArea("\t Total time it takes: " + time*2.5 + " minutes.");
                
                
                double totalAverageRatio= 0;  
                if (bestPath.size()>2){
                	appendToFastestPathTextArea("\t Total time it takes using speed limit: " + (bestPath.get(0).getSpeedTo(bestPath.get(1)))/70.0*time + " minutes.");
                }
                else{
                }
                appendToFastestPathTextArea("\n");
            }
        });
		
		
		jtaMessage= new JTextArea();
		jtaMessage.setEditable(false);
		JScrollPane jspMessagePanel= new JScrollPane(jtaMessage);
		jtaMessage.setBackground(jpMainFrame.getBackground());
		jtaMessage.setFont(new Font("Arial", Font.PLAIN, 10));
		jtaMessage.setWrapStyleWord(true);
		//jtaMessage.append("Hello cruel world!");
		
		addComponentIntoGridBayLayoutPanel(jpUserPanel, jlFrom, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpUserPanel, jcStreetChose1, 0, 1, 2, 1, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpUserPanel, jlTo, 0, 2, 1, 1, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpUserPanel, jcStreetChose2, 0, 3, 1, 2, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpUserPanel, jbFindFastestPath, 0, 4, 1, 1, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpUserPanel, jspMessagePanel, 0, 5, 1, 10, 1, 1, GridBagConstraints.BOTH);
		
		return jpUserPanel;
	}
	
	public void initComponents(){//Initiates frames and derives the frame space
		jtpTabMapGraph= new JTabbedPane();
		
		menuBar= new JMenuBar();
		jmAbout= new JMenu("About");
		jmOptions= new JMenu("Options");
		jmiOption1= new JMenuItem("Give Alan a cake");
		jmAbout1= new JMenuItem("Our team");
		
		jpMainFrame= new JPanel();
		jpMainFrame.setLayout(new GridBagLayout());
		
		sideFrame= new JPanel();
		sideFrame.setLayout(new GridBagLayout());
		sideFrame.setBorder(BorderFactory.createEtchedBorder());
		
		graphFrame= new JPanel();
		graphFrame.setLayout(new BorderLayout());
		graphFrame.setBorder(BorderFactory.createEtchedBorder());
		
		graphPane = new JTabbedPane();
		
		
		tableFrame = new JPanel();
		tableFrame.setLayout(new BorderLayout());
		tableFrame.setBorder(BorderFactory.createEtchedBorder());
		
		
		mapFrame= new JPanel();
		mapFrame.setLayout(new BorderLayout());
		mapFrame.setBorder(BorderFactory.createEtchedBorder());
		
		twitterFrame= new JPanel();
		twitterFrame.setLayout(new BorderLayout());
		twitterFrame.setBorder(BorderFactory.createEtchedBorder());
		
		additionSideFrame= new JPanel();
		setTrio(additionSideFrame);
		additionSideFrame.setLayout(new BorderLayout());
		additionSideFrame.setBorder(BorderFactory.createEtchedBorder());
		
		placeObjects();
	}

	public void addGraphToGraphPanel(){
		Dimension slightlySmaller= new Dimension(graphFrame.getWidth()-5, graphFrame.getHeight()-50);
		//graphFrame.add(new PieChart("Comparison","Speed of Cars", slightlySmaller), BorderLayout.CENTER);
		graphPane.addTab("Pie Chart",new PieChart("Comparison","Speed of Cars", slightlySmaller));
	}
	
	public void addLineChart(){
		Dimension ss= new Dimension(graphFrame.getWidth()-10, graphFrame.getHeight()-50);
		lineChart = new Graphs(ss);
		graphPane.addTab("Line Chart",lineChart);
		//graphPane.addTab("Line Chart",);
		
	}
	
	
	public void addHistoricalTable(){
		historicalTable = new HistoricalTable();
		tableFrame.add(historicalTable,BorderLayout.CENTER);
	}
	private void placeObjects() {//Puts all the panels in their respective 
		jtpTabMapGraph.addTab("Map Panel", mapFrame);

		jtpTabMapGraph.addTab("Graph Panel", graphFrame);
		
		jtpTabMapGraph.addTab("Table Panel", tableFrame);
		graphFrame.add(graphPane);
		
		menuBar.add(jmAbout);
		jmAbout.add(jmAbout1);
		
		menuBar.add(jmOptions);
		jmOptions.add(jmiOption1);
				
		addComponentIntoGridBayLayoutPanel(sideFrame, twitterFrame, 0, 0, 1, 3, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(sideFrame, additionSideFrame, 0, 1, 0, 7, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpMainFrame, jtpTabMapGraph, 0, 0, 7, 1, 1, 1, GridBagConstraints.BOTH);
		addComponentIntoGridBayLayoutPanel(jpMainFrame, sideFrame, 1, 0, 3, 1, 1, 1, GridBagConstraints.BOTH);

		this.add(menuBar, BorderLayout.NORTH);
		this.add(jpMainFrame, BorderLayout.CENTER);
		
		//Trying to patch and set the size of the combo boxes
		additionSideFrame.add(createUserInputPanel(namesOfRamps), BorderLayout.CENTER);
		
		addTwitterComponent(createTwitterPanel());
	}
	public void addMapComponent(JComponent mapComponent){//so that some one can call it from outside of the class 
		mapFrame.add(mapComponent, BorderLayout.CENTER);
		mapFrame.getRootPane().revalidate();
//		setVisible(true);
//		setResizable(false);
		
	}
	public void addTwitterComponent(JComponent twitterComponent){
		twitterFrame.add(twitterComponent, BorderLayout.CENTER);
	}
	public void addGraphComponent(JComponent graphComponent){
		//graphFrame.add(graphComponent, BorderLayout.CENTER);
		graphPane.addTab(String.valueOf(i), graphComponent);
		i++;
	}
	
	public void addComponentIntoGridBayLayoutPanel(JPanel jp, JComponent c, int gridx, int gridy, int weightx, int weighty, int width, int height, int fill){
		GridBagConstraints gbc= new GridBagConstraints();
		gbc.gridx= gridx;
		gbc.gridy= gridy;
		gbc.weightx= weightx;
		gbc.weighty= weighty;
		gbc.gridwidth= width;
		gbc.gridheight= height;
		gbc.fill= fill;
		jp.add(c, gbc);
	}
	public void importNamesOfRampes(){
		RampParser jason= new RampParser();
		namesOfRamps= jason.getNamesOfRamps();
		//System.out.println("File imported: "+ namesOfRamps.length);
//		for (int x= 0; x< namesOfRamps.length; x++){
//			System.out.println(namesOfRamps[x]);
//		}
	}
	
	public void setTrio(JPanel jp){
		jp.setPreferredSize(jp.getSize());
		jp.setMaximumSize(jp.getSize());
		jp.setMinimumSize(jp.getSize());
	}
	
	public static void main (String[] args){//Tester
		Gui Alan= new Gui();
		//Alan.pack();
		//we might need this 
		/*while(true){
			Alan.draw();
		}*/
	}
	
	public Graphs getLineChart() {
		return lineChart;
	}

	public void setLineChart(Graphs lineChart) {
		this.lineChart = lineChart;
	}

	public HistoricalTable getHistoricalTable() {
		return historicalTable;
	}

	public void setHistoricalTable(HistoricalTable historicalTable) {
		this.historicalTable = historicalTable;
	}


	
}
