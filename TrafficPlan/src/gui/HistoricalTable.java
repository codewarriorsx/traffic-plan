package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import Algorithm.Ramp;
import dbMgmt.DatabaseManager;
import dbMgmt.RampDataPackage;

public class HistoricalTable extends JPanel implements ItemListener{
	JComboBox highwayjcb,directionjcb,hoursjcb;
	DatabaseManager dbm;
	DefaultComboBoxModel model;
	String[] highwayOptions = {"101","10","405","110"};
	String[] nsDirection = {"North","South"};
	String[] ewDirection = {"East","West"};
	String[] hours = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"};
	String selectedFreeway;
	String selectedDirection;
	int selectedHour;
	List<RampDataPackage> rampList;
	JPanel selectionPanel= new JPanel();
	JPanel jpTextFrame= new JPanel();
	JTextArea jtaBoard= new JTextArea();
	/**
	 * Things to Remember
	 *  - updateData parses dbm and gives you a FULL LIST OF RAMPS WITH AVG SPEED CALCULATED BY HOUR
	 *  - create "GetHistoricalTable()" in gui class
	 *  - call "gui.GetHistoricalTable().setDbm(this);" in DatabaseManager class constructor
	 *  - create all necessary controls to select different Table Options
	 *  - create third "Table Panel" after Map Panel and Graph Panel in JTabbedPane
	 * */
	
	public HistoricalTable(){
		//updateData();//?
		this.setLayout(new BorderLayout());
		add(createSelectionPanel(), BorderLayout.CENTER);
		JButton jbexportToTextFile= new JButton("Export");
		jbexportToTextFile.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				WriteExcel jason= new  WriteExcel();
				jason.parse(jtaBoard.getText());
				jtaBoard.setText("");
				try {
					jason.writeIntoExcel();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		add(jbexportToTextFile, BorderLayout.SOUTH);
		
	}
	
	public void updateData(){
		
		try {
			rampList = dbm.rampDataRequest();
			updateDataPanel();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public JPanel createSelectionPanel(){
		selectionPanel = new JPanel();
		selectionPanel.setLayout(new BorderLayout());
		JPanel jpNorth= new JPanel();
		highwayjcb = new JComboBox(highwayOptions);
		highwayjcb.addItemListener(this);

		directionjcb = new JComboBox();
		directionjcb.addItemListener(this);
		model = new DefaultComboBoxModel(nsDirection);
		directionjcb.setModel(model);

		hoursjcb = new JComboBox(hours);
		hoursjcb.addItemListener(this);
		//direction.
		jpNorth.add(highwayjcb);
		jpNorth.add(directionjcb);
		jpNorth.add(hoursjcb);
		
		selectionPanel.add(jpNorth, BorderLayout.NORTH);
		updateDataPanel();
		jpTextFrame.setBorder(BorderFactory.createEtchedBorder());
		selectionPanel.add(jpTextFrame, BorderLayout.CENTER);
		
		return selectionPanel;
	}
	
	public void updateDataPanel(){
		jpTextFrame= new JPanel();
		System.out.println("HISTORY: "+ rampList);
		jpTextFrame.setLayout(new BorderLayout());
		if (rampList!= null){
			//System.out.println("Inside stupid text area");
			for (int x= 0; x< rampList.size(); x++){
				RampDataPackage r= rampList.get(x);
				if (highwayjcb.getSelectedItem().equals("101")){
					if (r.getHighwayName().equals("101")){
						jtaBoard.append(r.getRampName()+ ": \n");
					}
				}
				else if (highwayjcb.getSelectedItem().equals("10")){
					if (r.getHighwayName().equals("10")){
						jtaBoard.append(r.getRampName()+ ": \n");
					}
				}
				else if (highwayjcb.getSelectedItem().equals("405")){
					if (r.getHighwayName().equals("405")){
						jtaBoard.append(r.getRampName()+ ": \n");
					}
				}
				else if (highwayjcb.getSelectedItem().equals("110")){
					if (r.getHighwayName().equals("405")){
						jtaBoard.append(r.getRampName()+ ": \n");
					}
				}
			}
		}
		/*
		for (int x= 0; x< 100; x++){
			jtaBoard.append("Paul Way, 69 \n");
		}
		*/
	
		jtaBoard.setEditable(false);
		jpTextFrame.add(new JScrollPane(jtaBoard), BorderLayout.CENTER);
	}
	
	public void appendToBoard(String str){
		jtaBoard.append(str + "\n");
		jtaBoard.setCaretPosition(jtaBoard.getDocument().getLength());
		jtaBoard.getRootPane().revalidate();
	}
	
	public void itemStateChanged(ItemEvent ae) {
		
		if(ae.getSource().equals(highwayjcb)){
			if(highwayjcb.getSelectedItem().equals("10")){
				model = new DefaultComboBoxModel(ewDirection);
			}
			else{
				model = new DefaultComboBoxModel(nsDirection);
			}
			directionjcb.setModel(model);
			
		}
		
		selectedFreeway = (String) highwayjcb.getSelectedItem();
		selectedDirection = (String) directionjcb.getSelectedItem();
		selectedHour = Integer.valueOf((String) hoursjcb.getSelectedItem());
		
		updateData();
		this.getRootPane().revalidate();
	}

	


	public DatabaseManager getDbm() {
		return dbm;
	}

	public void setDbm(DatabaseManager dbm) {
		this.dbm = dbm;
	}

}
