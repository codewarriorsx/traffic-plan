package gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import universe.Brain;
import dbMgmt.CarDataPackage;

import javax.swing.*;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

public class PieChart extends JPanel {

  private static final long serialVersionUID = 1L;

  public PieChart(String applicationTitle, String chartTitle, Dimension size) {
        super();
        // This will create the dataset 
        PieDataset dataset = createDataset();
        // based on the dataset we create the chart
        JFreeChart chart = createChart(dataset, chartTitle);
        // we put the chart into a panel
        ChartPanel chartPanel = new ChartPanel(chart);
        // default size
        chartPanel.setPreferredSize(size);
        // add it to our application
        add(chartPanel);
    }
    
    
/**
     * Creates a sample dataset 
     */

    private  PieDataset createDataset() {
        DefaultPieDataset result = new DefaultPieDataset();
        int a, b, c;
        Random randall= new Random();
        a= randall.nextInt(10)+1;
        b= randall.nextInt(50- a)+ 50;
        c= 100- a - b;
        
        result.setValue("20 mph above speed limit", a);
        result.setValue("20 mph around speed limit", b);
        result.setValue("20 mph below speed limit", c);
        return result;
        
    }
    
    
/**
     * Creates a chart
     */

    private JFreeChart createChart(PieDataset dataset, String title) {
        
        JFreeChart chart = ChartFactory.createPieChart3D(title,          // chart title
            dataset,                // data
            true,                   // include legend
            true,
            false);

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
       // plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        plot.setBackgroundAlpha(1f);
        return chart;
        
    }
    
	public static void main(String[] args) {
		PieChart demo = new PieChart("Comparison",
				"Legality of Drivers?", new Dimension(100,100));
		JFrame blah= new JFrame();
		blah.add(demo);
		blah.setSize(100,100);
		blah.setVisible(true);
		blah.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

} 
