package gui;

import java.io.File;

import java.io.FileOutputStream;

import java.io.FileWriter;

import java.io.IOException;

import java.io.PrintWriter;

import java.util.ArrayList;

import java.util.Collection;

import java.util.HashMap;

import java.util.HashSet;

import java.util.Map;

import java.util.StringTokenizer;

public class WriteExcel
{
	public ArrayList<String> rampName;
	public ArrayList<Double> avgSpeed;
	public void parse(String s)
	{
		rampName = new ArrayList<String>();
		avgSpeed = new ArrayList<Double>();
		StringTokenizer jason = new StringTokenizer(s, "\n");
		while (jason.hasMoreTokens()) {
			StringTokenizer jason2 = new StringTokenizer(jason.nextToken(), ",");
			while (jason2.hasMoreTokens()) {
				rampName.add(jason2.nextToken());
				avgSpeed.add(Double.valueOf(jason2.nextToken()));
			}
		}
	}
	public void writeIntoExcel() throws IOException
	{
		FileWriter fw = new FileWriter("data.txt");
		PrintWriter pw = new PrintWriter(fw);
		for (int i = 0; i < rampName.size(); i++)
		{
			pw.print(rampName.get(i) + ", " + avgSpeed.get(i) + "\n");
		}
		pw.flush();
		pw.close();
		fw.close();
		System.out.println("File written to data.txt");
	}
}