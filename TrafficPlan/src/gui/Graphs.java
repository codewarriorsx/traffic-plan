package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.Dataset;
import org.joda.time.DateTime;

import dbMgmt.DatabaseManager;
import dbMgmt.RampDataPackage;

public class Graphs extends JPanel implements ItemListener{
	
	String title = "Current Avg Speed at Various Ramps By Highway";
	String[] highwayOptions = {"101","10","405","110"};
	String[] nsDirection = {"North","South"};
	String[] ewDirection = {"East","West"};
	String[] hours = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"};
	String selectedFreeway;
	String selectedDirection;
	int selectedHour;
	Map ramps;
	DatabaseManager dbm;
	JComboBox highwayjcb,directionjcb,hoursjcb;
	JPanel selectionPanel = new JPanel();
	DefaultCategoryDataset dcd;
	Dimension size;
	DefaultComboBoxModel model;
	
	public Graphs(Dimension size){
		super();
		dcd = new DefaultCategoryDataset();
		selectionPanel = createSelectionPanel(); 
		//selectionPanel.setVisible(false);
		
		add(selectionPanel);
		add(createChart(dcd,selectedFreeway));

		selectedFreeway = (String) highwayjcb.getSelectedItem();
		selectedDirection = (String) directionjcb.getSelectedItem();
		selectedHour = Integer.valueOf((String) hoursjcb.getSelectedItem());
		setPreferredSize(size);
	}
	
	
	@Override
	public void itemStateChanged(ItemEvent ae) {
		
		if(ae.getSource().equals(highwayjcb)){
			if(highwayjcb.getSelectedItem().equals("10")){
				model = new DefaultComboBoxModel(ewDirection);
			}
			else{
				model = new DefaultComboBoxModel(nsDirection);
			}
			directionjcb.setModel(model);
			
		}
		
		selectedFreeway = (String) highwayjcb.getSelectedItem();
		selectedDirection = (String) directionjcb.getSelectedItem();
		selectedHour = Integer.valueOf((String) hoursjcb.getSelectedItem());
		updateData(selectedFreeway,selectedDirection,selectedHour);
		repaint();
	}

	
	public JPanel createSelectionPanel(){
		JPanel selectionPanel = new JPanel();
		highwayjcb = new JComboBox(highwayOptions);
		highwayjcb.addItemListener(this);

		directionjcb = new JComboBox();
		directionjcb.addItemListener(this);
		model = new DefaultComboBoxModel(nsDirection);
		directionjcb.setModel(model);

		hoursjcb = new JComboBox(hours);
		hoursjcb.addItemListener(this);
		//direction.
		selectionPanel.add(highwayjcb);
		selectionPanel.add(directionjcb);
		selectionPanel.add(hoursjcb);
		return selectionPanel;
	}
	
	public ChartPanel createChart(CategoryDataset dataset, String freewayName){
		JFreeChart jfc = ChartFactory.createLineChart(title, "Ramp Name", "Average Speed (mph)", dataset,PlotOrientation.VERTICAL,false,false,false);	
		
		CategoryPlot p = jfc.getCategoryPlot(); 
		CategoryAxis axis = p.getDomainAxis();
		axis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);

		return new ChartPanel(jfc);
	}
	
	
	public void updateData(String freewayName, String direction, int hour){
		dcd.clear();
		//System.out.println(ramps.size());
		
		try {
			ramps = dbm.getRampSpeeds(freewayName, direction, hour);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!ramps.isEmpty()){
			for(Object s: ramps.keySet().toArray()){
				dcd.addValue((Double)ramps.get((String) s),"Speed",(String) s);
			}
			/*
			List<String> rampNames = new ArrayList<String>(ramps.keySet());
			List<Double> speeds = new ArrayList<Double>(ramps.values());
			for(int i = 0;i<rampNames.size();i++){
				dcd.addValue(speeds.get(i), "Speed", rampNames.get(i));
			}*/
		}
		else{
			System.err.println("Data for this time unavailable: Ramps List empty");
		}
	}
	
	public DatabaseManager getDbm() {
		return dbm;
	}

	public void setDbm(DatabaseManager dbm) {
		this.dbm = dbm;
		selectionPanel.setVisible(true);
	}

	public String getSelectedFreeway() {
		return selectedFreeway;
	}

	public void setSelectedFreeway(String selectedFreeway) {
		this.selectedFreeway = selectedFreeway;
	}

	public String getSelectedDirection() {
		return selectedDirection;
	}

	public void setSelectedDirection(String selectedDirection) {
		this.selectedDirection = selectedDirection;
	}

	public int getSelectedHour() {
		return selectedHour;
	}

	public void setSelectedHour(int selectedHour) {
		this.selectedHour = selectedHour;
	}
	
}
