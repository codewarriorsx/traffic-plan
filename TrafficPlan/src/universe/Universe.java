package universe;

import Map.Map;
import dbMgmt.DatabaseManager;
import gui.Gui;

public class Universe {



	Brain b;
	DatabaseManager dbm;

	public Universe(){


		Gui ttGui = new Gui();
		dbm = new DatabaseManager(ttGui);
		b = new Brain(dbm);
		dbm.setBrain(b);

		ttGui.addMapComponent(b.map);

		dbm.start();
	}


	public static void main(String[] args){
		Universe u = new Universe();

	}



}