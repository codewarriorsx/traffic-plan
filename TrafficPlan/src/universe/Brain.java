package universe;

import Algorithm.Ramp;
import Algorithm.RampParser;
import CarThreads.CarFactory;
import CarThreads.CarThread;
import Map.Map;
import dbMgmt.CarDataPackage;
import dbMgmt.DatabaseManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;

import dbMgmt.RampDataPackage;


/**
 * @author Nishant S
 * @version 1.0.1
 * @ReleaseDate: 4/20/2014
 * 
 * initially starts and generates a series of car threads 
 * 
 * @next version updates: improve run function, improve integration with gui, refactor
 */
public class Brain {
	
	DatabaseManager dbm;
	public static ArrayList<Ramp> ramp;
	public static List<CarDataPackage> carData;
	public List<RampDataPackage> rampData;
	public static HashMap<String, ArrayList<CarDataPackage>> carDataDictionary;
	public Map map;
	//public static HashMap<Long,CarThread> carDic;
	public CarDataPackage winners[];
	public RampParser rp;
	
	public Brain(DatabaseManager dbm){

		
		map = new Map();
		rp = new RampParser();
		this.dbm = dbm;
		//For Testing
		//testMode();

		
		initializeCars();
		
		winners = new CarDataPackage[3];
		carDataDictionary= new HashMap<String, ArrayList<CarDataPackage>>();
		carDataDictionary.clear(); //Removing all previous ones!
		
		for(int i=0; i< carData.size(); i++){
			long id = carData.get(i).getId();
			double speed = carData.get(i).getSpeed();
			int directionNum = carData.get(i).getDirectionNum();
			String ramp = carData.get(i).getRamp();
			String freeway = carData.get(i).getFreeway();
			if(speed < 90) {
                CarThread carthread = new CarThread(rp, freeway, ramp, directionNum, speed, map.carFactory, id);
                map.carFactory.addCar(carthread);
            }
			
			System.out.println(map.carFactory.cars.size());
			//Addition roads to the dictionary
			if (carDataDictionary.containsKey(ramp)){
				carDataDictionary.get(ramp).add(carData.get(i));
			}
			else{
				ArrayList<CarDataPackage>listOfCarsInRamp= new ArrayList<CarDataPackage>();
				listOfCarsInRamp.add(carData.get(i));
				carDataDictionary.put(ramp, listOfCarsInRamp);
			}
			
		/*	if(carData.get(i).getSpeed()>70){
				int indexOfSlowest= -1;
				double slowestSpeed=  Double.MAX_VALUE;
				for (int x= 0; x< 3; x++){
					if (winners[x]== null){
						winners[x]= carData.get(i);
					}
					else if (winners[x].getSpeed()< slowestSpeed){
						slowestSpeed= winners[x].getSpeed();
						indexOfSlowest= x;
					}
				}
				if (indexOfSlowest!= -1){
					winners[indexOfSlowest]= carData.get(i);
				}
			}*/
			
			
		}
		
		
		/*

		for (int x= 0; x< 3; x++){
			if (winners[x]!= null){
				try {
					TwitterDispatcher.update(winners[x]);
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}*/
		//cars = Collections.synchronizedList(new ArrayList<CarThread>());
		//adding car to car factory
		
		
	}
	
	/*
	 * interactions with DBMgr
	 * */
	public void testMode(){
		dbm.interrupt();
		int hour = 1;
		int minute = 37;
		try {
			carData =  dbm.carDataRequest(hour,minute);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	public void initializeCars() {

		dbm.interrupt();
		DateTime jodaDate = new DateTime();
		int hour = jodaDate.getHourOfDay();
		int minute = jodaDate.getMinuteOfHour();
		try {
			dbm.loadDataFromServer(hour,minute);
			carData =  dbm.carDataRequest(hour,minute);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loadCars(List<CarDataPackage> cars){
		carData =  cars;
		for(int i=0; i< carData.size(); i++){
			long id = carData.get(i).getId();
			double speed = carData.get(i).getSpeed();
			int directionNum = carData.get(i).getDirectionNum();
			String ramp = carData.get(i).getRamp();
			String freeway = carData.get(i).getFreeway();
			if(map.carFactory.cars.size()>0){
				map.carFactory.cars.get(i).setCar(freeway, ramp, directionNum, speed);
			}
		}	
		
		
/*
			if(carData.get(i).getSpeed()>70){
				int indexOfSlowest= -1;
				double slowestSpeed=  Double.MAX_VALUE;
				for (int x= 0; x< 3; x++){
					if (winners[x]== null){
						winners[x]= carData.get(i);
					}
					else if (winners[x].getSpeed()< slowestSpeed){
						slowestSpeed= winners[x].getSpeed();
						indexOfSlowest= x;
					}
				}
				if (indexOfSlowest!= -1){
					winners[indexOfSlowest]= carData.get(i);
				}
			}*/
			
			
			
			
		
		/*
		for (int x= 0; x< 3; x++){
			if (winners[x]!= null){
				try {
					TwitterDispatcher.update(winners[x]);
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}*/

	}

	public DatabaseManager getDbm() {
		return dbm;
	}

	public void setDbm(DatabaseManager dbm) {
		this.dbm = dbm;
	}

    public static List<CarDataPackage> getCarData() {
        return carData;
    }
}
