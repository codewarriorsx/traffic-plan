package Algorithm;

import Map.LatLongPoint;
import gui.Gui;
import dbMgmt.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

/**
 * @author Jason Han
 * 
 */

public class Dijkstra {
	
	/**
	 * 
	 * @param nodeArr All information for ramps should be contained, the edges that is connected to the edges, and the edge weight
	 * @param s Start point of the path
	 * @param t End point of the path
	 * @param bestPath return value. The best path from s to t.
	 * @return the length of the best path
	 */
	
	Map<String, RampDataPackage> ramps;
	
	public Dijkstra(){
		System.out.println("Dijktra's open");
	}
	
	public static double calcBestPath(Vector<Ramp> nodeArr, Ramp s, Ramp t, Vector<Ramp> bestPath) {
		double totalTime= 0;
		Vector<Ramp> unvisited = new Vector<Ramp>();
		System.out.println(nodeArr.get(0).equals(s));

		for (int i = 0; i < nodeArr.size(); i++) {
			if (nodeArr.get(i).equals(s))
				nodeArr.get(i).setWeightFromSource(0);
			else nodeArr.get(i).setWeightFromSource(Double.MAX_VALUE);
			unvisited.add(nodeArr.get(i));
		}

		while (!unvisited.isEmpty()) {
			System.out.println("It's inside " + unvisited.size());
			Ramp minNode = new Ramp("", "", null);
			minNode.setWeightFromSource(Double.MAX_VALUE);
			// this for loop selects out the minimum weight value node from the
			// starting node
			for (int i = 0; i < unvisited.size(); i++) {
				//System.out.println(unvisited.get(i).getWeightFromSource() + "----->>>>" + minNode.getWeightFromSource());
				if (unvisited.get(i).getWeightFromSource() < minNode.getWeightFromSource()) {
					minNode = unvisited.get(i);/*
					System.out.println("fuckuckfuck");
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
				}
			}
			if (minNode.getRampName().equals(""))
				break;
			System.out.println("====");
			System.out.println(minNode.getRampName() + " " + minNode.getFreewayNumber());
			//System.out.println(minNode.getLatitude() + "-----" + minNode.getLongitude());
			System.out.println(minNode.getWeightFromSource());

			for (Edge e : minNode.getEdges()) {
				Ramp neighbor = null;
				if (e.getN1() == minNode)
					neighbor = e.getN2();
				else
					neighbor = e.getN1();
				/*
				if (neighbor.getWeightFromSource() > minNode.getWeightFromSource() + e.getWeight()) {
					neighbor.setWeightFromSource(e.getWeight()+ minNode.getWeightFromSource());
					neighbor.setPre(minNode);
				}
				*/
				System.out.println("Edge weight to node: "+ minNode.getWeightTo(neighbor));
				if (neighbor.getWeightFromSource() > minNode.getWeightFromSource() + minNode.getWeightTo(neighbor)) {
					neighbor.setWeightFromSource(minNode.getWeightTo(neighbor)+ minNode.getWeightFromSource());
					neighbor.setPre(minNode);
				}
			}
			unvisited.remove(minNode);
		}

		if (t.getWeightFromSource() == Double.MAX_VALUE) {
			System.out.println("no path");
			return Double.MAX_VALUE;
		}
		bestPath.add(t);
		Ramp tmp = t;
		System.out.println(s.getRampName() + " " + s.FreeWayNum);
		System.out.println(s.getLatitude() + " " + s.getLongitude());
		System.out.println(t.getRampName() + " " + t.FreeWayNum);
		System.out.println(t.getLatitude() + " " + t.getLongitude());
		System.out.println(s.equals(t));
		while (!tmp.equals(s)) {
			bestPath.add(0, tmp.getPre());
			tmp = tmp.getPre(); //Get parent node?
		}

		String[] roadMap= new String[bestPath.size()];
		for (int x= 0; x< bestPath.size(); x++){
			roadMap[x]= bestPath.get(x).rampName;
		}

		for (int x= 0; x< bestPath.size()-1; x++){
			totalTime+= (bestPath.get(x).getWeightTo(bestPath.get(x+1)));
		}
		//Gui.appendToFastestPathTextArea(roadMap);
		return totalTime;
	}

}
