package Algorithm;

/**
 * @author Jason Han 
 *
 */

import Map.LatLongPoint;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class RampParser {

	public Vector<LatLongPoint> RampList = new Vector<LatLongPoint>();
	public static Vector<Ramp> Ramps = new Vector<Ramp>();
	public static Vector<Ramp> I10 = new Vector<Ramp>();
	public static Vector<Ramp> I101 = new Vector<Ramp>();
	public static Vector<Ramp> I405 = new Vector<Ramp>();
	public static Vector<Ramp> I105 = new Vector<Ramp>();
	public static HashMap<String, Ramp> rampDictionary;

	// ArrayList<LatLongPoint> RampList = new ArrayList<LatLongPoint>();
	// ArrayList<LatLongPoint> RampList = new ArrayList<LatLongPoint>();
	static int counter = 0;

	public RampParser() {
		rampDictionary = new HashMap<String, Ramp>();
		try {
			FileReader fr = new FileReader("ramps.txt");
			BufferedReader br = new BufferedReader(fr);
			String line = " ";
			while (br.ready()) {
				line = br.readLine().trim();
				counter++;
				if (!line.isEmpty()) {

					// counter++;
					// line = br.readLine();
					if (line.equals("10")) {
						String RampName = br.readLine();
						// System.out.println("10");
						// System.out.println(RampName);
						line = br.readLine();
						double la = Double.parseDouble(line);
						// System.out.println(la);
						line = br.readLine();
						double lo = Double.parseDouble(line);
						// System.out.println(lo);
						LatLongPoint gp = new LatLongPoint(la, lo);
						RampList.add(gp);
						Integer i = 10;
						String s = Integer.toString(i);
						Ramp r = new Ramp(s, RampName, gp);
						Ramps.add(r);
						I10.add(r);
						rampDictionary.put("I" + r.getFreewayNumber() + ": "
								+ r.getRampName(), r);
					}
					if (line.equals("101")) {
						String RampName = br.readLine();
						// System.out.println("101");
						// System.out.println(RampName);
						line = br.readLine();
						double la = Double.parseDouble(line);
						// System.out.println(la);
						line = br.readLine();
						double lo = Double.parseDouble(line);
						// System.out.println(lo);

						LatLongPoint gp = new LatLongPoint(la, lo);
						RampList.add(gp);
						Integer i = 101;
						String s = Integer.toString(i);
						Ramp r = new Ramp(s, RampName, gp);
						Ramps.add(r);
						I101.add(r);
						rampDictionary.put("I" + r.getFreewayNumber() + ": "
								+ r.getRampName(), r);

					}
					if (line.equals("405")) {
						// System.out.println("405");
						String RampName = br.readLine();
						// System.out.println(RampName);
						line = br.readLine();
						double la = Double.parseDouble(line);
						// System.out.println(la);
						line = br.readLine();
						double lo = Double.parseDouble(line);
						// System.out.println(lo);
						LatLongPoint gp = new LatLongPoint(la, lo);
						RampList.add(gp);
						Integer i = 405;
						String s = Integer.toString(i);
						Ramp r = new Ramp(s, RampName, gp);
						Ramps.add(r);
						I405.add(r);
						rampDictionary.put("I" + r.getFreewayNumber() + ": "
								+ r.getRampName(), r);

					}
					if (line.equals("105")) {
						// System.out.println("105");
						String RampName = br.readLine();
						// System.out.println(RampName);
						line = br.readLine();
						double la = Double.parseDouble(line);
						// System.out.println(la);
						line = br.readLine();
						double lo = Double.parseDouble(line);
						// System.out.println(lo);
						LatLongPoint gp = new LatLongPoint(la, lo);
						RampList.add(gp);
						Integer i = 105;
						String s = Integer.toString(i);
						Ramp r = new Ramp(s, RampName, gp);
						Ramps.add(r);
						I105.add(r);
						rampDictionary.put("I" + r.getFreewayNumber() + ": "
								+ r.getRampName(), r);

					} else {// todo ??

					}
				}
				// System.out.println(Ramps.size());
			}

		} catch (IOException ioe) {
			// System.out.println(ioe.getMessage());
			ioe.printStackTrace();
		}
		
		//add neighbors for four main freeways
		for (int i = 0; i < I101.size()-1; i++) {
			Ramp first = I101.get(i);
			Ramp second = I101.get(i+1);
			first.insertNeighbor(second, first.getDistanceTo(second), first.getSpeedTo(second));
			second.insertNeighbor(first, second.getDistanceTo(first), second.getSpeedTo(first));
		}
		
		for (int i = 0; i < I10.size()-1; i++) {
			Ramp first = I10.get(i);
			Ramp second = I10.get(i+1);
			first.insertNeighbor(second, first.getDistanceTo(second), first.getSpeedTo(second));
			second.insertNeighbor(first, second.getDistanceTo(first), second.getSpeedTo(first));
		}
		
		for (int i = 0; i < I105.size()-1; i++) {
			Ramp first = I105.get(i);
			Ramp second = I105.get(i+1);
			first.insertNeighbor(second, first.getDistanceTo(second), first.getSpeedTo(second));
			second.insertNeighbor(first, second.getDistanceTo(first), second.getSpeedTo(first));
		}
		
		for (int i = 0; i < I405.size()-1; i++) {
			Ramp first = I405.get(i);
			Ramp second = I405.get(i+1);
			first.insertNeighbor(second, first.getDistanceTo(second), first.getSpeedTo(second));
			second.insertNeighbor(first, second.getDistanceTo(first), second.getSpeedTo(first));
		}
		
		//add neighbors for the intersections of freeways 
		Ramp inter10_101_first = I101.get(7), inter10_101_second = I10.get(32);
		inter10_101_first.insertNeighbor(inter10_101_second, inter10_101_first.getDistanceTo(inter10_101_second), inter10_101_first.getSpeedTo(inter10_101_second));
		inter10_101_second.insertNeighbor(inter10_101_first, inter10_101_second.getDistanceTo(inter10_101_first), inter10_101_second.getSpeedTo(inter10_101_first));
		
		Ramp inter101_405_first = I101.get(42), inter101_405_second = I405.get(19);
		inter101_405_first.insertNeighbor(inter101_405_second, inter101_405_first.getDistanceTo(inter101_405_second), inter101_405_first.getSpeedTo(inter101_405_second));
		inter101_405_second.insertNeighbor(inter101_405_first, inter101_405_second.getDistanceTo(inter101_405_first), inter101_405_second.getSpeedTo(inter101_405_first));
		
		Ramp inter10_405_first = I10.get(7), inter10_405_second = I405.get(9);
		inter10_405_first.insertNeighbor(inter10_405_second, inter10_405_first.getDistanceTo(inter10_405_second), inter10_405_first.getSpeedTo(inter10_405_second));
		inter10_405_second.insertNeighbor(inter10_405_first, inter10_405_second.getDistanceTo(inter10_405_first), inter10_405_second.getSpeedTo(inter10_405_first));
	
		Ramp inter105_405_first = I105.get(4), inter105_405_second = I405.get(0);
		inter105_405_first.insertNeighbor(inter105_405_second, inter105_405_first.getDistanceTo(inter105_405_second), inter105_405_first.getSpeedTo(inter105_405_second));
		inter105_405_second.insertNeighbor(inter105_405_first, inter105_405_second.getDistanceTo(inter105_405_first), inter105_405_second.getSpeedTo(inter105_405_first));
	}

	public String[] getNamesOfRamps() {
		String[] listOfRampNames = new String[Ramps.size()];
		for (int x = 0; x < Ramps.size(); x++) {
			listOfRampNames[x] = "I" + Ramps.get(x).getFreewayNumber() + ": "
					+ Ramps.get(x).getRampName();
		}
		return listOfRampNames;
	}

	public Vector<Ramp> getRamp() {
		return Ramps;
	}

	public Vector<Ramp> getI10() {
		return I10;
	}

	public Vector<Ramp> getI101() {
		return I101;
	}

	public Vector<Ramp> getI405() {
		return I405;
	}

	public Vector<Ramp> getI105() {
		return I105;
	}

	public static void main(String args[]) {
		RampParser rp = new RampParser();
		Vector<Ramp> bestPath = new Vector<Ramp>();
		
		
		Dijkstra.calcBestPath(Ramps, I105.get(0), I10.get(0), bestPath);
		
		System.out.println("fuck");
		for (int i = 0; i < bestPath.size()-1; i++) {
			System.out.println(bestPath.get(i).getRampName() + "---->" + bestPath.get(i).getFreewayNumber());
			System.out.println("time-----"+ bestPath.get(i).getWeightTo(bestPath.get(i+1)));

		}
		System.out.println(bestPath.get(bestPath.size()-1).getRampName() + "---->" + bestPath.get(bestPath.size()-1).getFreewayNumber());

		// System.out.println(counter);
	}

}
