package Algorithm;

import Map.LatLongPoint;

import java.util.ArrayList;
import java.util.Vector;

import universe.Brain;
import dbMgmt.CarDataPackage;

/**
 * @author Jason Han
 *
 */

public class Ramp {
	int id;
	ArrayList<Edge> connectEdges;
	double weightFromSource;
	Ramp pre;
	double averageSpeed;
	int numberOfCars;
	
	String FreeWayNum;
	private final LatLongPoint point;
	String rampName;
	Ramp EN;
	Ramp SW;
	

	public Ramp(String FreeWayNum, String rampName, LatLongPoint point) {
		this.FreeWayNum = FreeWayNum;
		this.rampName = rampName;
		this.point = point;
		connectEdges = new ArrayList<Edge>();
		weightFromSource = Double.MAX_VALUE;
		pre = null;
	}
	public LatLongPoint getLatLongPoint()
	{
		return point;
	}
	
	public void setEN (Ramp EN)
	{
		this.EN = EN;
	}
	
	public void setSW (Ramp SW)
	{
		this.SW = SW;
	}
	public Ramp getEN ()
	{
		return EN;
	}
	public Ramp getSW()

	{
		return SW;
	}
	public String getFreewayNumber()
	{
		return FreeWayNum;	
	}
	public String getRampName()
	{
		return rampName;
	}
	public double getLongitude()
	{
		return point.getLongitude();
	}
	public double getLatitude()
	{
		return point.getLatitude();
	}
	
	public void setAverageSpeed(double _averageSpeed)
	{
		averageSpeed = _averageSpeed;
	}
	public void setNumberOfCars( int _numberOfCars)
	{
		numberOfCars = _numberOfCars;
	}
	public double getAverageSpeed()
	{
		return averageSpeed;
	}
	public int getNumberOfCars()
	{
		return numberOfCars;
	}
	
	public double getWeightFromSource() {
		return this.weightFromSource;
	}

	public void setWeightFromSource(double dist) {
		this.weightFromSource = dist;
	}

	public Ramp getPre() {
		return pre;
	}

	public void setPre(Ramp _pre) {
		pre = _pre;
	}

	public ArrayList<Edge> getEdges() {
		return connectEdges;
	}

	//should be modified this part based on how to read the ramp file.
	public void insertNeighbor(Ramp node, double dis, double speed) {
		//this line means that this is a double edged graph 
		Edge edge = new Edge(this, node, dis);
		connectEdges.add(edge);
	}

	public int getId() {
		return id;
	}

	public void setName(int _id) {
		id = _id;
	}
	
	public double getDistanceTo(Ramp b){ //How to do this? Need a function
		//TODO can make better by adding up smaller segments given by the legs class
		//Reference: http://www.movable-type.co.uk/scripts/latlong.html
		double R = 6371; // km
		double dLat = Math.toRadians(b.getLatitude()-this.getLatitude());
		double dLon = Math.toRadians(b.getLongitude()- this.getLongitude());
		double lat1 = Math.toRadians(this.getLatitude());
		double lat2 = Math.toRadians(b.getLatitude());
		
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		double d = R * c;
		return d*0.621371;
	}
	
	public int getDirectionTo(Ramp b){
		
		return 0;
	}
	
	

	
	public double getSpeedTo(Ramp b){
		double answer= 70; //Normal speed, when no car exists in this node towards b

		int directionOfCarsToNodeB= 0;
		ArrayList<CarDataPackage> allCarsInNode = null;
		System.out.println("Car data Dictioanry: "+ Brain.carDataDictionary);
		System.out.println("Car ramp: "+ RampParser.Ramps);
		if (Brain.carDataDictionary!= null){
			if (Brain.carDataDictionary.containsKey(rampName)){
				allCarsInNode= Brain.carDataDictionary.get(rampName);
			}
			else{
				System.out.println("Does not contain key "+ rampName);
			}
			Vector<Ramp> allRamps=RampParser.Ramps;
			
			int currentRampIndex= -1;
			for (int x= 0; x< allRamps.size(); x++){
				if (allRamps.get(x).getRampName().equalsIgnoreCase(this.rampName)){
					currentRampIndex= x;
					break;
				}
			}
			if (currentRampIndex!= -1){
				System.out.println("CurrentRampIndex: "+ currentRampIndex);
				if (currentRampIndex-1>= 0 && currentRampIndex-1< allRamps.size() && allRamps.get(currentRampIndex-1).getRampName().equalsIgnoreCase(b.getRampName())){
					directionOfCarsToNodeB= -1;
				}
				else if (currentRampIndex-1>= 0 && currentRampIndex+1< allRamps.size() && allRamps.get(currentRampIndex+ 1).getRampName().equalsIgnoreCase(b.getRampName())){
					directionOfCarsToNodeB= 1;
				}
			}
		}
		System.out.println("Direction " + directionOfCarsToNodeB);
		System.out.println("AllCarsInNode: "+ allCarsInNode);
		
		double totalSpeed= 0;
		double totalCars= 0;
		if (allCarsInNode!= null){
			if (directionOfCarsToNodeB== 1 || directionOfCarsToNodeB== -1){
				for (int x = 0; x < allCarsInNode.size(); x++) {
					if (allCarsInNode.get(x).getDirectionNum() == directionOfCarsToNodeB) { //Segment directions
						totalSpeed += allCarsInNode.get(x).getSpeed();
						totalCars++;
					}
				}
				if (totalCars!= 0){
					answer= totalSpeed/ totalCars;
				}	
			}
		}
		System.out.println(answer);
		return answer;
	}

	public double getWeightTo(Ramp b){
		return (getDistanceTo(b)/ getSpeedTo(b))*60;
	}
	
	public int hashCode() {
		return 1;
	}
	
	public boolean equals(Object _node) {
		if (!(_node instanceof Ramp))
			return false;
		Ramp node = (Ramp)_node;
		double epsilon = 0.000001;
		if (Math.abs(this.getLatitude() - node.getLatitude()) <= epsilon && Math.abs(this.getLongitude() - node.getLongitude()) <= epsilon) {
			return true;
		}
		return false;
	}
	

}
