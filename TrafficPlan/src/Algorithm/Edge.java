package Algorithm;

/*
 * @author Jason Han
 */

public class Edge {

	private double speed;
	private double distance;
	private double edgeWeight;
	private Ramp n1, n2;

	public Edge(Ramp n1, Ramp n2, double d) {
		distance = d;
		this.n1 = n1;
		this.n2 = n2;
	}

	public void setSpeed(double s) {
		speed = s;
		edgeWeight = distance/s;
	}

	public double getSpeed() {
		return speed;
	}

	public void setDistance(double d) {
		distance = d;
	}

	public double getDistance() {
		return distance;
	}

	public double getWeight() {
		return edgeWeight;
	}

	public Ramp getN1() {
		return n1;
	}

	public Ramp getN2() {
		return n2;
	}
}
