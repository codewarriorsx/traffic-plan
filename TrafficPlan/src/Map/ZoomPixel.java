package Map;

import java.awt.*;

/**
 * Created by Daniel Rowland on 4/19/2014.
 */
public class ZoomPixel extends Point{
    int zoomFactor;
    ZoomPixel(int x, int y, int zoomFactor){
        super(x,y);
        this.zoomFactor = zoomFactor;
    }

    @Override
    public String toString() {
        return "ZoomPixel{" +
                "zoomFactor=" + zoomFactor +
                "} " + super.toString();
    }

    public int getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(int zoomFactor) {
        this.zoomFactor = zoomFactor;
    }
}
