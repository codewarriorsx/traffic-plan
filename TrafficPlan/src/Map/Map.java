package Map;

import javax.imageio.ImageIO;
import CarThreads.CarFactory;
import javax.swing.*;
import CarThreads.CarFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Map extends JPanel {

	LatLongPoint center;
	int zoomFactor;
	BufferedImage mapImage;
	String urlString;
	String imageFileName = "mapImage.jpg";
	int mapWidth;
	int mapHeight;
	int scale;
	final int MAX_ZOOM = 19;
	final int MIN_ZOOM = 9;
	final double MIN_LAT = 36.018875; // increases as you go North
	final double MAX_LAT = 33.064962;
	final double MIN_LON = -120.284310;// decreases as you go West
	final double MAX_LON = -117.207749;
	URL imageURL;
	InputStream is;
	OutputStream os;
	Dimension panelDimension;
	JButton zoomIn, zoomOut, panLeft, panRight, panUp, panDown;
	// all cars location
	public static ArrayList<LatLongPoint> pointArr = new ArrayList<LatLongPoint>();
	public CarFactory carFactory;
	MapThread thread;

	public Map() {

		carFactory = new CarFactory(this);

		center = new LatLongPoint(34.040926, -118.246888);
		zoomFactor = 9;
		scale = 1;
		mapWidth = 640;
		mapHeight = 640;
		panelDimension = new Dimension(mapWidth, mapHeight);
		setPreferredSize(panelDimension);
		updateMap();
		Dimension buttonDimension = new Dimension(30, 30);
		zoomIn = new JButton("+");
		zoomIn.setPreferredSize(buttonDimension);
		zoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (zoomFactor < MAX_ZOOM) {
					zoomFactor++;
					updateMap();
				}
			}
		});
		zoomOut = new JButton("-");
		zoomOut.setPreferredSize(buttonDimension);
		zoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (zoomFactor > MIN_ZOOM) {
					zoomFactor--;
					updateMap();
				}
			}
		});
		panLeft = new JButton("<");
		panLeft.setPreferredSize(buttonDimension);
		panLeft.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (center.getLongitude() > MIN_LON) {
					center.setLongitude(center.getLongitude()
							- (.5 / zoomFactor));
					updateMap();
				}
			}
		});
		panRight = new JButton(">");
		panRight.setPreferredSize(buttonDimension);
		panRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (center.getLongitude() < MAX_LON) {
					center.setLongitude(center.getLongitude()
							+ (.5 / zoomFactor));
					updateMap();
				}
			}
		});
		panUp = new JButton("^");
		panUp.setPreferredSize(buttonDimension);
		panUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (center.getLatitude() < MAX_LAT) {
					center.setLatitude(center.getLatitude() + (.5 / zoomFactor));
					updateMap();
				}
			}
		});

		panDown = new JButton("v");
		panDown.setPreferredSize(buttonDimension);
		panDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (center.getLatitude() > MIN_LAT) {
					center.setLatitude(center.getLatitude() - (.5 / zoomFactor));
					updateMap();
				}
			}
		});
		setLayout(new FlowLayout());
		add(zoomIn);
		add(zoomOut);
		add(panLeft);
		add(panRight);
		add(panUp);
		add(panDown);
		
		thread = new MapThread(this);
		thread.start();

	}
	
	public int getMapWidth(){
		return mapWidth;
	}
	
	public int getMapHeight(){
		return mapHeight;
	}
	
	public void run(){
		while(true){
			repaint();
		}
	}

	private void updateMap() {
		String markerStr = "";
		for (int i = 0; i < pointArr.size(); i++) {
			markerStr += "&markers=color:blue%7Clabel:S%7C"
					+ pointArr.get(i).getLatitude() + ","
					+ pointArr.get(i).getLongitude();
		}

		// urlString =
		// "http://maps.googleapis.com/maps/api/staticmap?center="+center.getLatitude()+","+center.getLongitude()+"&scale="+scale
		// +"&markers="+ "color:blue%7Clabel:S%7C34.0315456,-118.2602429"
		// +"&markers="+ "color:blue%7Clabel:S%7C34.0252647,-118.4657498"
		// +"&zoom="+zoomFactor+"&size="+mapWidth+"x"+mapHeight+"&key=AIzaSyCiRhFCJDLCcANitmL57L9ATDPt-BSaDT8";
		urlString = "http://maps.googleapis.com/maps/api/staticmap?center="
				+ center.getLatitude() + "," + center.getLongitude()
				+ "&scale=" + scale + markerStr + "&zoom=" + zoomFactor
				+ "&size=" + mapWidth + "x" + mapHeight
				+ "&key=AIzaSyCiRhFCJDLCcANitmL57L9ATDPt-BSaDT8";

		try {
			imageURL = new URL(urlString);
			is = imageURL.openStream();
			os = new FileOutputStream(imageFileName);
			byte[] b = new byte[2048];
			int length;

			while ((length = is.read(b)) != -1) {
				os.write(b, 0, length);
			}
			is.close();
			os.close();
			mapImage = ImageIO.read(new File(imageFileName));
		} catch (MalformedURLException murle) {
			murle.printStackTrace();
			System.out.println(murle.getMessage());
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}
		this.repaint();
	}
	
	

	@Override
	protected void paintComponent(Graphics g) {

		g.drawImage(mapImage, 0, 0, 640, 640, null);
		carFactory.carDraw(g);
		
	}

	public int getZoomFactor() {
		// TODO Auto-generated method stub
		return zoomFactor;
	}

	public LatLongPoint getCenter() {
		// TODO Auto-generated method stub
		return center;
	}


}

class MapThread extends Thread{
	Map map;
	MapThread(Map map){
		this.map = map;
	}
	
	public void run(){
		while(true){
			map.repaint();
		}
	}
}