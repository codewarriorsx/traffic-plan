package Map;

import Algorithm.Ramp;

/**
 * Created by Daniel Rowland on 4/16/2014.
 */
public class LatLongPoint{
    double latitude;
    double longitude;

    public LatLongPoint(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "LatLongPoint{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
    





}
