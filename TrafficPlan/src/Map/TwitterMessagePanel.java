package Map;

import twitter4j.Status;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Daniel Rowland on 4/13/2014.
 */
public class TwitterMessagePanel extends JPanel {
    Status status;
    Dimension panelDimension;

    TwitterMessagePanel(Status status){
        super();
        this.status = status;
        this.setBorder(BorderFactory.createEtchedBorder());
        this.add(new JLabel(status.getText()));
        //panelDimension = new Dimension(400,200);
        //setSize(panelDimension);
        //setPreferredSize(panelDimension);
    }

    /*
    public void drawString(Graphics g, String s, int x, int y, int width)
    {
        // FontMetrics gives us information about the width,
        // height, etc. of the current Graphics object's Font.
        FontMetrics fm = g.getFontMetrics();

        int lineHeight = fm.getHeight();

        int curX = x;
        int curY = y;

        String[] words = s.split(" ");

        for (String word : words)
        {
            // Find out thw width of the word.
            int wordWidth = fm.stringWidth(word + " ");

            // If text exceeds the width, then move to next line.
            if (curX + wordWidth >= x + width)
            {
                curY += lineHeight;
                curX = x;
            }

            g.drawString(word, curX, curY);

            // Move over to the right for next word.
            curX += wordWidth;
        }
    }
     
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        g.fillRoundRect(0,0,this.getWidth(),this.getHeight(),15,15);
        g.setColor(Color.BLACK);
        drawString(g, status.getText(), 20, 20, 200);
    }
    */

    public static void main(String[] args){
        JFrame frame = new JFrame("Test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800,600);
        frame.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        constraints.weightx = 1;
        constraints.weighty = 1;
        constraints.gridheight =1;
        constraints.gridwidth = 1;
        constraints.gridx = 0;
        constraints.gridy = 0;
        TwitterDispatcher td = new TwitterDispatcher();
        TwitterMessagePanel test = new TwitterMessagePanel(td.list.get(0));
        frame.add(test,constraints);
        frame.setVisible(true);
    }
}
