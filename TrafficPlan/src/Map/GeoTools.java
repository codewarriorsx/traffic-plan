package Map;

import java.awt.*;
import java.awt.Point;


/**
 * Created by Daniel Rowland on 4/19/2014.
 */
public class GeoTools{
    private static final double EarthRadius = 6378137;
    private static final double MinLatitude = -85.05112878;
    private static final double MaxLatitude = 85.05112878;
    private static final double MinLongitude = -180;
    private static final double MaxLongitude = 180;



    // Clips a number to the specified minimum and maximum values.
    private static double Clip(double n, double minValue, double maxValue)
    {
        return Math.min(Math.max(n, minValue), maxValue);
    }




    // Determines the map width and height (in pixels) at a specified level
    // of detail.
    public static int MapSize(int zoomFactor)
    {
        return  (int) (256 * Math.pow(2, ((Integer)zoomFactor).doubleValue()));
    }




    // Finds the meters per pixel at a specified latitude and zoomFactor.
    public static double metersPerPixel(double latitude, int zoomFactor)
    {
        latitude = Clip(latitude, MinLatitude, MaxLatitude);
        return EarthRadius * Math.cos(latitude * Math.PI / 180)/(Math.pow(2,zoomFactor+8)); // uncomment for radian mode,
        //return EarthRadius * Math.cos(latitude)/(Math.pow(2,zoomFactor+8));// uncomment for degree mode
    }




    // Determines the map scale at a specified latitude, level of detail,
    // and screen resolution.

//    public static double MapScale(double latitude, int zoomFactor, int screenDpi)
//    {
//        return metersPerPixel(latitude, zoomFactor) * screenDpi / 0.0254;
//    }




    // Converts LatLongPoint from latitude/longitude WGS-84 coordinates (in degrees)
    // into ZoomPixels.
    public static ZoomPixel latLongPointToZoomPixel(LatLongPoint llp, int zoomFactor)
    {
        double latitude = Clip(llp.getLatitude(), MinLatitude, MaxLatitude);
        double longitude = Clip(llp.getLongitude(), MinLongitude, MaxLongitude);

        double x = (longitude + 180) / 360;
        double sinLatitude = Math.sin(latitude * Math.PI / 180);
        double y = 0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);

        int mapSize = MapSize(zoomFactor);
        int pixelX = (int) Clip(x * mapSize + 0.5, 0, mapSize - 1);
        int pixelY = (int) Clip(y * mapSize + 0.5, 0, mapSize - 1);
        return new ZoomPixel(pixelX,pixelY,zoomFactor);
    }

    public static GlobalPixel latLongPointToGlobalPixel(LatLongPoint llp){
        return (GlobalPixel) latLongPointToZoomPixel(llp, 0);
    }



    // Converts a pixel from pixel XY coordinates at a specified level of detail
    // into latitude/longitude WGS-84 coordinates (in degrees).

    public static LatLongPoint zoomPixelToLatLongPoint(ZoomPixel zp, int zoomFactor)
    {
        double mapSize = MapSize(zoomFactor);
        double x = (Clip(zp.getX(), 0, mapSize - 1) / mapSize) - 0.5;
        double y = 0.5 - (Clip(zp.getY(), 0, mapSize - 1) / mapSize);

        double latitude = 90 - 360 * Math.atan(Math.exp(-y * 2 * Math.PI)) / Math.PI;
        double longitude = 360 * x;
        return new LatLongPoint(latitude,longitude);
    }

    public static LatLongPoint globalPixelToLatLongPoint(GlobalPixel gp){
        return zoomPixelToLatLongPoint(gp,0);
    }

    public static Point convertLatLongToLocalXY(LatLongPoint llp,Map map){
        //System.out.println("Point to plot: "+ llp);
        ZoomPixel pointOfInterest = latLongPointToZoomPixel(llp,map.getZoomFactor());
        LatLongPoint mapCenter = map.getCenter();
       // System.out.println("Mapcenter: " + mapCenter);
        ZoomPixel viewPortCenter = latLongPointToZoomPixel(mapCenter, map.getZoomFactor());
       // System.out.println("ViewportCenter: "+ viewPortCenter);
        int topLeftX =(int) (viewPortCenter.getX()-(map.getMapWidth()/2));
        int topLeftY = (int) (viewPortCenter.getY() - (map.getMapHeight()/2));
        //int bottomRightX =(int) (viewPortCenter.getX() + (map.getMapWidth()/2));
       // int bottomRightY =(int) (viewPortCenter.getY() + (map.getMapHeight()/2));
       // ZoomPixel topLeftZoomPixel = new ZoomPixel(topLeftX,topLeftY,map.getZoomFactor());
        //System.out.println("topleftzoom: "+ topLeftZoomPixel);
      //  ZoomPixel bottomRightZoomPixel = new ZoomPixel(bottomRightX,bottomRightY,map.getZoomFactor());
       // System.out.println("BottomRightZoom:" + bottomRightZoomPixel);
//        BoundingBox bb = new BoundingBox(topLeftZoomPixel,bottomRightZoomPixel);
//        if(!bb.contains(pointOfInterest.getX(),pointOfInterest.getY())){
//            return new Point(-5,-5);
//        }
        
//        LatLongPoint topLeftLL = zoomPixelToLatLongPoint(topLeftZoomPixel,map.getZoomFactor() );
//        LatLongPoint bottomRightLL = zoomPixelToLatLongPoint(bottomRightZoomPixel,map.getZoomFactor());

        int x = (int) pointOfInterest.getX() - topLeftX;
        int y = (int) pointOfInterest.getY() - topLeftY;
        return new Point(x,y);

    }






}
