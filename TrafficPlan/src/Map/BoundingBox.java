package Map;

import java.awt.*;

/**
 * Created by Daniel Rowland on 4/20/2014.
 */
public class BoundingBox {
    private Interval intervalX;  // x-axis interval
    private Interval intervalY;  // y-axis interval

    // smallest bounding box containing (x1, y1) and (x2, y2)
    public BoundingBox(Point p1, Point p2) {
        intervalX = new Interval(Math.min(p1.getX(), p2.getX()), Math.max(p1.getX(), p2.getX()));
        intervalY = new Interval(Math.min(p1.getY(), p2.getY()), Math.max(p1.getY(), p2.getY()));
    }

    // smallest bounding box containing the two points
    public BoundingBox(Interval x, Interval y) {
        intervalX = x;
        intervalY = y;
    }


    // smallest bounding box containing all the points
    public BoundingBox(Point[] points) {
        double xmin = Double.POSITIVE_INFINITY;
        double ymin = Double.POSITIVE_INFINITY;
        double xmax = Double.NEGATIVE_INFINITY;
        double ymax = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < points.length; i++) {
            if (points[i].getX() < xmin) xmin = points[i].getX();
            if (points[i].getY() < ymin) ymin = points[i].getY();
            if (points[i].getX() > xmax) xmax = points[i].getX();
            if (points[i].getY() > ymax) ymax = points[i].getY();
        }
        intervalX = new Interval(xmin, xmax);
        intervalY = new Interval(ymin, ymax);
    }


    // is (x, y) inside this BoundingBox?
    public boolean contains(double x, double y) {
        return intervalX.contains(x) && intervalY.contains(y);
    }

    // does this BoundingBox intersect b?
    public boolean intersects(BoundingBox b) {
        return intervalX.intersects(b.intervalX) &&
                intervalY.intersects(b.intervalY);
    }

    // smallest bounding box containing this bounding box and b
    public BoundingBox union(BoundingBox b) {
        return new BoundingBox(intervalX.union(b.intervalX),
                intervalY.union(b.intervalY));
    }

    // return the width, height and area
    public double width()  { return intervalX.length(); }
    public double height() { return intervalY.length(); }
    public double area()   { return width() * height(); }

    // return a string representation
    public String toString() {
        return "[ " + intervalX + " " + intervalY + " ]";
    }


}
