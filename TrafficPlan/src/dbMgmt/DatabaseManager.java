package dbMgmt;

import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import universe.Brain;
import gui.Gui;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.*;
import java.util.*;

import Map.TwitterDispatcher;


/*
 * Database Manager Class
 * @Author Nishant Srikanthan
 * 
 * aggregate current speeds of a car by freeway
 * combine those averages with historical value
 * 
 * RELEASE DATE: 4/26/2014
 * VERSION 1.2.0
 * Reads data from online server and stores data in sql db
 * 
 *
 */


public class DatabaseManager extends Thread{
	
	
	Brain b;
	String tableName = "traffic_plan_table";
	Saver saver= new Saver();
	Calendar rightNow;
	boolean exit = false;
	private ResultSet rst;
	URL url;
	BufferedReader reader = null;
	Map<String, RampDataPackage> ramps;
    TwitterDispatcher snitch;
	
	/*
	 * DB Setings
	 * */
	private Connection con = null;
	private PreparedStatement preparedStatement = null;
	
	private final String DB_NAME = "traffic_plan_db";
	String ipAddress = "25.174.90.29";
	String userName = "root";
	String password = "CODEWARRIORS2014";

	//Local Testing
	//private final String DB_NAME = "traffic-plan";
	//String ipAddress = "localHost";
	//String password = "we12re34";
	
	String driverName = "com.mysql.jdbc.Driver";
	String dburl = "jdbc:mySql://"+ipAddress+":3306/"+DB_NAME;
	Gui gui;




	public DatabaseManager(Gui gui){
			this.setPriority(8);

		this.gui = gui;
		gui.getLineChart().setDbm(this);
		gui.getHistoricalTable().setDbm(this);
		
		snitch = new TwitterDispatcher();

		try{
			
			//Initialize Database -- Create Table
			Class.forName(driverName);
			//Get Connection to Online Server
			url = new URL("http://www-scf.usc.edu/~csci201/mahdi_project/project_data.json");
			createCarTable();
			
			
			
		}catch(Exception e){
			System.err.println("DBMGR Constructor Exception: ");
			e.printStackTrace();
		}
			
		String freeway = gui.getLineChart().getSelectedFreeway();
		String direction = gui.getLineChart().getSelectedDirection();
		int hour = gui.getLineChart().getSelectedHour();
		
		try {
			System.out.println(getRampSpeeds(freeway,direction,hour));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
	}
	
	
	public void run(){


		while(!exit){
			synchronized(this){
				try {
                    this.wait(1000*60*3);
					updateCars();
					

				} catch (InterruptedException e) {
					System.out.println("DBMgr was rudely awakened");
					//e.printStackTrace();
				} catch(Exception e){
					System.out.println(e);
				}
			}
		}
		
		
	}
	
	
	/*
	 * 
	 * DB MGR Rework
	 * 
	 * **/
		
	/*
	 * Creates Table
	 * */
	
	public void createCarTable() throws SQLException {
		
		//Create statement to be executed
		String createString =
		        "CREATE TABLE IF NOT EXISTS "+tableName+"(HOUR_OF_DAY int," +
		        "MINUTE_OF_HOUR int," +
		        "CAR_ID bigint," +
		        "SPEED double, " +
		        "DIRECTION varchar(10), " +
		        "RAMP varchar(200), " +
		        "FREEWAY varchar(100), " +
		        "PRIMARY KEY (CAR_ID,HOUR_OF_DAY,MINUTE_OF_HOUR))";	            
		
	    Statement stmt = null;
	    try {
			con = DriverManager.getConnection(dburl,userName,password); 
	        stmt = con.createStatement();
	        stmt.executeUpdate(createString);
	        System.out.println("Table "+tableName+ " Created");

	    } catch (SQLException e) {
	    	System.out.println("JDBC Create Table Error: ");
	    	e.printStackTrace();
	    	throw(e);
	    } finally {
	        if (stmt != null) { stmt.close(); }
	    }
	}
	
	/*
	 * Update Calls
	 * 
	 * 
	 * */
	
	//Cars
	public void  updateCars(){
		DateTime jodaDate = new DateTime();
		int hour = jodaDate.getHourOfDay();
		int minute = jodaDate.getMinuteOfHour();

		try{

            snitch.spyOnUpdate(b.getCarData());
            loadDataFromServer(hour,minute);
            b.loadCars(carDataRequest(hour,minute));
            snitch.spyOnUpdate(b.getCarData());
        }catch(Exception e){
            e.printStackTrace();
        }

	}
	
	
	/*
	 * LOAD FROM ONLINE SERVER
	 * */
	public int loadDataFromServer(int hour, int minute) throws Exception{

		reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));

		int status = 0;
		preparedStatement = null;
			
		try{
			con = DriverManager.getConnection(dburl,userName,password); 
			JSONParser parser = new JSONParser();
			JSONArray a = (JSONArray) parser.parse(reader);
			
			preparedStatement = con.prepareStatement("REPLACE INTO "+tableName+" values(?,?,?,?,?,?,?)");
			int i = 0;
			for (Object o : a)
			  {
			    JSONObject car = (JSONObject) o;
			    //Get Variables from file
			    Long id = (Long) car.get("id");
			    Double speed = (Double) car.get("speed");
			    String direction = (String) car.get("direction");
			    String ramp = (String) car.get(String.valueOf("on/off ramp"));    
			    String freeway = (String) car.get("freeway");

		    	preparedStatement.setInt(1,hour);
		    	preparedStatement.setInt(2,minute);
			    preparedStatement.setLong(3,id);
			    preparedStatement.setDouble(4,speed);
			    preparedStatement.setString(5,direction);
			    preparedStatement.setString(6,ramp);
			    preparedStatement.setString(7,freeway);
			    preparedStatement.executeUpdate();
			    
			    if(i%100 == 0){
			    	
			    	System.out.println(i/10 + "% DB Load Complete");
			    }
			    
			    i++;
			  }
			
			System.out.println("Values Inserted into DB");
			
		}catch(Exception e){
			throw(e);
		}finally{
			try{
			 if (con != null) {
	             con.close();
	           }

	         } catch (Exception e) {

	         }
		}
		
		
		reader.close();
		gui.getHistoricalTable().getRootPane().revalidate();
		return status;
	
	
	}
	
	

	/*
	 * Calls  
	 * */
	
	
	public List<CarDataPackage> carDataRequest(int hour, int minute) throws SQLException{
		
		List<CarDataPackage> cars = new ArrayList<CarDataPackage>();
		preparedStatement = null;
		
		
		try{
			con = DriverManager.getConnection(dburl,userName,password); 
			 
			preparedStatement = con.prepareStatement("SELECT HOUR_OF_DAY, MINUTE_OF_HOUR, CAR_ID, FREEWAY, DIRECTION, RAMP, SPEED FROM "+tableName+" GROUP BY HOUR_OF_DAY, MINUTE_OF_HOUR, CAR_ID" );
			rst = preparedStatement.executeQuery();
			long carID = 0;

			while(rst.next()){
				if(rst.getInt("HOUR_OF_DAY")==(hour)&&rst.getInt("MINUTE_OF_HOUR")==(minute) && rst.getLong("CAR_ID")== carID ){
					cars.add(new CarDataPackage(rst.getLong("CAR_ID"),rst.getDouble("SPEED"),rst.getString("direction"),rst.getString("ramp"),rst.getString("freeway")));
					carID++;
				}
			}
			
		}catch(SQLException e){
			throw(e);
		}finally{
			try{
			 if (con != null) {
	             con.close();
	           }

	         } catch (Exception e) {

	         }
		}
		
		return cars;
	}
	
	//rampDataRequest(int,int)
	
	public List<RampDataPackage> rampDataRequest() throws SQLException{
		//KEY IN FORMAT "[FREEWAY]:[RAMP]"
		//List<RampDataPackage> ramps = new ArrayList<RampDataPackage>();
		
		List<RampDataPackage> allRamps = new ArrayList<RampDataPackage>();
		
		preparedStatement = null;

		try{
			con = DriverManager.getConnection(dburl,userName,password); 
			 
			preparedStatement = con.prepareStatement("SELECT HOUR_OF_DAY, FREEWAY, DIRECTION, RAMP, AVG(SPEED) FROM "+tableName+" GROUP BY HOUR_OF_DAY, FREEWAY, DIRECTION, RAMP" );
			rst = preparedStatement.executeQuery();
			
			while(rst.next()){
				RampDataPackage rdp = new RampDataPackage(rst.getString("FREEWAY"),rst.getString("RAMP"),rst.getString("DIRECTION"),rst.getDouble("AVG(SPEED)"),rst.getInt("HOUR_OF_DAY"));
				allRamps.add(rdp);				
			}
			
		}catch(SQLException e){
			throw(e);
		}finally{
			try{
			 if (con != null) {
	             con.close();
	           }

	         } catch (Exception e) {

	         }
		}
		return allRamps;
	}
	
	
	public Map<String, Double> getRampSpeeds(String freewayName, String direction, int hour) throws SQLException{
		
		Map<String, Double> ramps = new HashMap<String, Double>();
				
		preparedStatement = null;

		
		try{
			con = DriverManager.getConnection(dburl,userName,password); 
			 
			preparedStatement = con.prepareStatement("SELECT HOUR_OF_DAY, FREEWAY, DIRECTION, RAMP, AVG(SPEED) FROM "+tableName+" GROUP BY FREEWAY, DIRECTION, RAMP, HOUR_OF_DAY" );
			rst = preparedStatement.executeQuery();
			while(rst.next()){
				if(rst.getInt("HOUR_OF_DAY")==(hour)&&rst.getString("FREEWAY").equals(freewayName)&&rst.getString("DIRECTION").equals(direction)){
					ramps.put(rst.getString("RAMP"), rst.getDouble("AVG(SPEED)"));
				}
			}
			
		}catch(SQLException e){
			throw(e);
		}finally{
			try{
			 if (con != null) {
	             con.close();
	           }

	         } catch (Exception e) {

	         }
		}
		return ramps;

	}
	//rampDataRequest(int,int)
	
	public Map<String, RampDataPackage> historicalRampDataRequest(int hour) throws SQLException{
		//KEY IN FORMAT "[FREEWAY]:[RAMP]"
		//List<RampDataPackage> ramps = new ArrayList<RampDataPackage>();
		
		Map<String, RampDataPackage> allRamps = new HashMap<String, RampDataPackage>();
		

		
		preparedStatement = null;

		
		try{
			con = DriverManager.getConnection(dburl,userName,password); 
			 
			preparedStatement = con.prepareStatement("SELECT HOUR_OF_DAY, FREEWAY, DIRECTION, RAMP,AVG(SPEED) as avg_speed FROM "+tableName+" GROUP BY FREEWAY, RAMP, DIRECTION, HOUR_OF_DAY" );
			rst = preparedStatement.executeQuery();
			while(rst.next()){
				if(rst.getInt("HOUR_OF_DAY")==(hour)){
					RampDataPackage rdp = new RampDataPackage(rst.getString("FREEWAY"),rst.getString("RAMP"),rst.getString("DIRECTION"),rst.getDouble("avg_speed"), rst.getInt("HOUR_OF_DAY"));
					allRamps.put(rst.getString("FREEWAY")+":"+rst.getString("RAMP"), rdp);
				}
			}
			
		}catch(SQLException e){
			throw(e);
		}finally{
			try{
			 if (con != null) {
	             con.close();
	           }

	         } catch (Exception e) {

	         }
		}
		return allRamps;
	}	
	/*
	 * Use this to get Historical traffic condition at each ramp
	 */
	
	/**********************************************************************************/
	public Double getAvgSpeedAtRamp(String rampName, String freeway, String direction, int hour) throws SQLException{
		preparedStatement = null;
		double output = -1.0;
		try{
			con = DriverManager.getConnection(dburl,userName,password); 
			 
			preparedStatement = con.prepareStatement("SELECT HOUR_OF_DAY, FREEWAY, DIRECTION, RAMP,AVG(SPEED) FROM "+tableName+" GROUP BY HOUR_OF_DAY, FREEWAY, DIRECTION, RAMP" );
			rst = preparedStatement.executeQuery();
			
			while(rst.next()){
				if(rst.getString("RAMP").equals(rampName) && rst.getString("DIRECTION").equals(direction) && rst.getInt("HOUR_OF_DAY")==(hour)){
					output = rst.getDouble("AVG(SPEED)");
				}
			}
			
		}catch(SQLException e){
			throw(e);
		}finally{
			try{
			 if (con != null) {
	             con.close();
	           }

	         } catch (Exception e) {

	         }
		}
		

		
		return output;
	}
	
		
	/*
	 *
	 * Utilities
	 * 
	 */
	
	
	
	public Brain getBrain() {
		return b;
	}


	public void setBrain(Brain b) {
		this.b = b;
	}


	public boolean isExit() {
		return exit;
	}


	public void setExit(boolean exit) {
		this.exit = exit;
	}
	

	
}
