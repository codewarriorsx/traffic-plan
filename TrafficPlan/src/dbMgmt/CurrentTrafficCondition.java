package dbMgmt;

import java.io.Serializable;

/**
 * @author Nishant S
 *
 */
public class CurrentTrafficCondition implements Serializable {

	String highway;
	String rampName;
	String direction;
	Double speedNE;
	Double speedSW;
	int carIndex;
	int hourOfDay;
	
	public CurrentTrafficCondition(String highway, String rampName, String direction, Double speedNE, Double speedSW, int carIndex, int hourOfDay){
		this.highway = highway;
		this.rampName = rampName;
		this.direction = direction;
		this.speedNE = speedNE;
		this.speedSW = speedSW;
		this.carIndex = carIndex;
		this.hourOfDay = hourOfDay;
	}

	public String getHighway() {
		return highway;
	}

	public void setHighway(String highway) {
		this.highway = highway;
	}

	public Double getSpeedNE() {
		return speedNE;
	}

	public void setSpeedNE(Double speedNE) {
		this.speedNE = speedNE;
	}
	
	public Double getSpeedSW() {
		return speedSW;
	}

	public void setSpeedSW(Double speedSW) {
		this.speedSW = speedSW;
	}

	public int getCarIndex() {
		return carIndex;
	}

	public void setCarIndex(int carIndex) {
		this.carIndex = carIndex;
	}

	public int getTimeOfDay() {
		return hourOfDay;
	}

	public void setTimeOfDay(int hourOfDay) {
		this.hourOfDay = hourOfDay;
	}

	public String getRampName() {
		return rampName;
	}

	public void setRampName(String rampName) {
		this.rampName = rampName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	
}