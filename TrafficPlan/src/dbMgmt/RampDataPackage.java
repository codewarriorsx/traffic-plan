package dbMgmt;

public class RampDataPackage {
	
	private String highwayName,rampName;
	private double speedNE = -1.0;
	private double speedSW = -1.0;
	int hour;
	
	public RampDataPackage(String highwayName, String rampName, String direction, Double speed, int hour){
		this.highwayName = highwayName;
		this.rampName = rampName;
		if(direction == "north" || direction == "east"){
			speedNE = speed;
		}
		else{
			speedSW = speed;
		}
		this.hour = hour;
		
	}

	public String getHighwayName() {
		return highwayName;
	}

	public void setHighwayName(String highwayName) {
		this.highwayName = highwayName;
	}

	public String getRampName() {
		return rampName;
	}

	public void setRampName(String rampName) {
		this.rampName = rampName;
	}

	public double getSpeedNE() {
		return speedNE;
	}

	public void setSpeedNE(double speedNE) {
		this.speedNE = speedNE;
	}

	public double getSpeedSW() {
		return speedSW;
	}

	public void setSpeedSW(double speedSW) {
		this.speedSW = speedSW;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}
	
}
