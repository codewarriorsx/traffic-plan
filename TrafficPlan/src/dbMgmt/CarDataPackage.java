package dbMgmt;
/*
 * 
 * @Author: Nishant Srikanthan
 * CarDataPackage
 * Class 
 * 
 * */
public class CarDataPackage {

	Long id;
	double speed;
	String direction,ramp,freeway;

	int directionNum;
	
	public CarDataPackage(Long id, double speed, String direction,String ramp,String freeway){
		this.id = id;
		this.speed = speed;
		if(direction == "N" || direction == "E"){
			directionNum = 0;
		}
		else{
			directionNum = 1;
		}
		this.ramp = ramp;
		this.freeway = freeway;
	}
	
	
	
	/*
	 * Getters/Setters 
	 * 
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
	public int getDirectionNum() {
		return directionNum;
	}

	public void setDirection(int directionNum) {
		this.directionNum = directionNum;
	}

	
	public String getRamp() {
		return ramp;
	}

	public void setRamp(String ramp) {
		this.ramp = ramp;
	}

	public String getFreeway() {
		return freeway;
	}

	public void setFreeway(String freeway) {
		this.freeway = freeway;
	}

	public void printCar(){
		//for testing
		System.out.println("Car " +id+" Freeway: "+freeway+" Speed: "+ speed + " ramp: " + ramp);
	}
	
}
