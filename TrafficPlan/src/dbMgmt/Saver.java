package dbMgmt;

import java.io.*;


/**
 * @author wangalan
 *
 */
public class Saver {
	private int iterator;
	private final int maxThreshold = 24;
	public Saver(){
		iterator= 0;
	}
	
	public void saveFile(Object o) throws IOException{ //Saved based on iterator, saves carlistdata packages in dataparse.java
		ObjectOutputStream oos= new ObjectOutputStream(new FileOutputStream(String.valueOf(iterator)));
		oos.writeObject(o);
		oos.close();
		iterator++;
		if (iterator> maxThreshold){ //To account for overlap
			iterator= 0;
		}
	}
	
	public void saveFile(Object o, String savepath) throws IOException{
		ObjectOutputStream oos= new ObjectOutputStream(new FileOutputStream(savepath));
		oos.writeObject(o);
		oos.close();
	}
	
	public Object readFile(String savedpath) throws FileNotFoundException, IOException, ClassNotFoundException{
		ObjectInputStream ois= new ObjectInputStream(new FileInputStream(savedpath));
		Object o= ois.readObject();
		ois.close();
		return o;
	}
}
